<?php

use App\Models\SimulationTier\Slug;
use App\Repositories\SlugRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery\Mockery;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class SlugRepositoryTestConcrete extends SlugRepository
{
    public function model()
    {
        return 'TestModel';
    }
}

class SlugRepositoryTestModel extends Model
{
    public function where($a, $is, $b) {
    }

    public function get() {
    }
}

class SlugRepositoryTest extends TestCase
{
    /**
     * @var SlugRepository
     */
    protected $slugRepo;

    public function setUp()
    {
        parent::setUp();

        $slug = 'caviar';

        $this->model = $this->createMock(SlugRepositoryTestModel::class);
        App::instance('TestModel', $this->model);
        $this->slugRepo = new SlugRepositoryTestConcrete(App::make('app'));
    }

    /**
     * @test find-by-slug
     */
    public function testFindBySlug()
    {
        $slug = 'caviar';

        $this->model->where = function ($a, $is, $b) {};
        $this->model
             ->expects($this->once())
             ->method('where')
             ->with($this->equalTo('slug'), $this->equalTo('='), $this->equalTo($slug))
             ->will($this->returnSelf());

        $coll = new Collection([$this->model]);
        $this->model
             ->expects($this->once())
             ->method('get')
             ->will($this->returnValue($coll));

        $result = $this->slugRepo->findBySlug($slug);

        $this->assertEquals($result, $this->model);
    }
}
