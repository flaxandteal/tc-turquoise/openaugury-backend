<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\SimulationTier\Simulation;
use Symfony\Component\HttpFoundation\Response;
use App\Transformers\SimulationTransformer;

class SimulationApiTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return Simulation::class;
    }

    public function getTransformer()
    {
        return new SimulationTransformer;
    }

    public function getJsonFields()
    {
        return [
            'settings',
            'center',
            'extent',
            'begins',
            'ends',
            'owner_id',
            'result',
            'status',
            'case_context_id'
        ];
    }

    /**
     * @test
     */
    public function testCreateSimulation()
    {
        $simulation = $this->fake();

        $simulationJson = $this->reduceToJsonFields($simulation->toArray());

        $simulationJson['settings'] = json_decode($simulationJson['settings'], true);
        $simulationJson['result'] = json_decode($simulationJson['result'], true);
        $simulationJson['combination_id'] = $simulation->combination->id;
        $simulationJson['definition'] = $simulation->definition;

        $this->json('POST', '/api/v1/simulations', $simulationJson);

        unset($simulationJson['combination_id']);
        unset($simulationJson['definition']);

        $this->assertApiResponse($simulationJson);
    }

    /**
     * @test
     */
    public function testReadSimulation()
    {
        $simulation = $this->make();

        $simulationJson = $this->reduceToJsonFields($simulation->toArray());

        $simulationJson['settings'] = json_decode($simulationJson['settings'], true);
        $simulationJson['result'] = json_decode($simulationJson['result'], true);

        $this->json('GET', '/api/v1/simulations/' . $simulation->id, $simulationJson);

        $this->assertApiResponse($simulationJson);
    }

    /**
     * @test
     */
    public function testUpdateSimulation()
    {
        $simulation = $this->make();

        $editedSimulation = $this->fakeData();

        $editedSimulationJson = $this->reduceToJsonFields($editedSimulation);

        $editedSimulationJson['settings'] = json_decode($editedSimulationJson['settings'], true);
        $editedSimulationJson['result'] = json_decode($editedSimulationJson['result'], true);

        $this->json('PUT', '/api/v1/simulations/'.$simulation->id, $editedSimulationJson);

        $this->assertApiResponse($editedSimulationJson);
    }

    /**
     * @test
     */
    public function testDeleteSimulation()
    {
        $simulation = $this->make();

        $this->json('DELETE', '/api/v1/simulations/' . $simulation->id);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->json('GET', '/api/v1/simulations/' . $simulation->id);

        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }
}
