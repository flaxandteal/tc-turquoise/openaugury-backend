<?php

use App\Models\CaseTier\CaseContext;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CaseContextApiTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return CaseContext::class;
    }

    public function getTransformer()
    {
        return new CaseContextTransformer;
    }

    public function getJsonFields()
    {
        return [
            'name',
            'center',
            'extent',
            'district_id',
            'begins',
            'ends',
            'owner_id',
        ];
    }

    /**
     * @test
     */
    public function testCreateCaseContext()
    {
        $caseContext = $this->fakeData();

        $caseContextJson = $this->reduceToJsonFields($caseContext);

        $this->json('POST', '/api/v1/case-contexts', $caseContextJson);

        $this->assertApiResponse($caseContextJson);
    }

    /**
     * @test
     */
    public function testReadCaseContext()
    {
        $caseContext = $this->make();

        $caseContextJson = $this->reduceToJsonFields($caseContext->toArray());

        $this->json('GET', '/api/v1/case-contexts/' . $caseContext->id);

        $this->assertApiResponse($caseContextJson);
    }

    /**
     * @test
     */
    public function testUpdateCaseContext()
    {
        $caseContext = $this->make();

        $editedCaseContext = $this->fakeData();

        $this->json('PUT', '/api/v1/case-contexts/' . $caseContext->id, $editedCaseContext);

        $this->assertApiResponse($editedCaseContext);
    }

    /**
     * @test
     */
    public function testDeleteCaseContext()
    {
        $caseContext = $this->make();

        $this->json('DELETE', '/api/v1/case-contexts/'. $caseContext->id);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->json('GET', '/api/v1/case-contexts/' . $caseContext->id);

        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }
}
