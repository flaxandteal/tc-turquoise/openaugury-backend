<?php

use App\Models\AbstractTier\Phenomenon;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PhenomenonApiTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return Phenomenon::class;
    }

    public function getTransformer()
    {
        return new PhenomenonTransformer;
    }

    public function getJsonFields()
    {
        return [
            'name',
            'symbol',
            'color',
        ];
    }

    /**
     * @test
     */
    public function testCreatePhenomenon()
    {
        $phenomenon = $this->fakeData();

        $phenomenonJson = $this->reduceToJsonFields($phenomenon);

        $this->json('POST', '/api/v1/phenomenons', $phenomenon);

        $this->assertApiResponse($phenomenonJson);
    }

    /**
     * @test
     */
    public function testReadPhenomenon()
    {
        $phenomenon = $this->make();

        $phenomenonJson = $this->reduceToJsonFields($phenomenon->toArray());

        $this->json('GET', '/api/v1/phenomenons/' . $phenomenon->slug);

        $this->assertApiResponse($phenomenonJson);
    }

    /**
     * @test
     */
    public function testUpdatePhenomenon()
    {
        $phenomenon = $this->make();

        $editedPhenomenon = $this->fakeData();

        $editedPhenomenon = $this->reduceToJsonFields($editedPhenomenon);

        $this->json('PUT', '/api/v1/phenomenons/' . $phenomenon->slug, $editedPhenomenon);

        $this->assertApiResponse($editedPhenomenon);
    }

    /**
     * @test
     */
    public function testDeletePhenomenon()
    {
        $phenomenon = $this->make();

        $this->json('DELETE', '/api/v1/phenomenons/'.$phenomenon->slug);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->json('GET', '/api/v1/phenomenons/'.$phenomenon->slug);

        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }
}
