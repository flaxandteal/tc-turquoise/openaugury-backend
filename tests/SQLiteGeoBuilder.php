<?php

class SQLiteGeoBuilder extends \Illuminate\Database\Schema\Builder
{
    /**
     * Create a new command set with a Closure.
     *
     * @param string $table
     * @param Closure $callback
     * @return Blueprint
     */
    protected function createBlueprint($table, Closure $callback = null)
    {
        return new SQLiteGeoBlueprint($table, $callback);
    }
}
