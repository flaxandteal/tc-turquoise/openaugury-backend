<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Webpatser\Uuid\Uuid;

use App\Transformers\SimulationTransformer;
use App\Models\SimulationTier\Simulation;
use App\Models\CaseTier\CaseContext;
use App\Models\AbstractTier\Combination;
use App\Models\AbstractTier\NumericalModel;
use App\Models\AbstractTier\Phenomenon;

class SimulationTransformerTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return Simulation::class;
    }

    public function getTransformer()
    {
        return new SimulationTransformer;
    }

    /**
     * Ensure that a simulation can provide standard information in JSON.
     *
     * @return void
     */
    public function testSimulationHasBasicInfo()
    {
        $simulation = $this->make();

        $transformer = new SimulationTransformer;
        $response = json_encode($transformer->transform($simulation));

        $value = json_decode($response);

        $this->assertEquals($value->id, $simulation->id);
        $this->assertEquals($value->settings, json_decode($simulation->settings));
        $this->assertEquals($value->extent, $simulation->extent);
        $this->assertEquals(Carbon::parse($value->begins), $simulation->begins);
        $this->assertEquals(Carbon::parse($value->ends), $simulation->ends);
        $this->assertEquals($value->owner_id, $simulation->owner_id);
        $this->assertEquals($value->result, json_decode($simulation->result));
        $this->assertEquals(Carbon::parse($value->created_at), $simulation->created_at);
        $this->assertEquals(Carbon::parse($value->updated_at), $simulation->updated_at);
    }

    /**
     * Check that the phenomenon ID is included with a simulation
     *
     * @return void
     */
    public function testSimulationHasPhenomenonId()
    {
        $simulation = $this->make();

        $combination = new Combination;
        $combination->id = (string)Uuid::generate();
        $numericalModel = new NumericalModel;
        $numericalModel->id = (string)Uuid::generate();
        $phenomenon = new Phenomenon;
        $phenomenon->id = 9;
        $phenomenon->slug = 'ban-ana-s';

        $simulation->combination = $combination;
        $simulation->combination_id = $combination->id;
        $combination->numericalModel = $numericalModel;
        $combination->numerical_model_id = $numericalModel->id;
        $numericalModel->phenomenon = $phenomenon;
        $numericalModel->phenomenon_id = $phenomenon->id;

        $transformer = new SimulationTransformer;
        $response = json_encode($transformer->transform($simulation));

        $value = json_decode($response);

        $this->assertEquals($value->phenomenon, $phenomenon->slug);
    }

    /**
     * Check that the case context ID is included with a simulation
     *
     * @return void
     */
    public function testSimulationHasCaseContextId()
    {
        $simulation = $this->make();

        $caseContext = new CaseContext;
        $caseContext->id = (string)Uuid::generate();

        $simulation->combination = $caseContext;
        $simulation->case_context_id = $caseContext->id;

        $transformer = new SimulationTransformer;
        $response = json_encode($transformer->transform($simulation));

        $value = json_decode($response);

        $this->assertEquals($value->case_context_id, $caseContext->id);
    }
}
