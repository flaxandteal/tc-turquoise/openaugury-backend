<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Login' => 'Login',
    'Remember Me' => 'Remember Me',
    'Password' => 'Password',
    'EMail Address' => 'E-Mail Address',
    'Forgot Your Password?' => 'Forgot Your Password?',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
