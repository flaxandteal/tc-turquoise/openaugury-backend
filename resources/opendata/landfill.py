import ne_to_latlng
import pandas

def run():
    csv_name = 'authorised-landfill-sites-ni-2015'
    csv_file = pandas.read_csv(csv_name + '.csv')

    converter = ne_to_latlng.IrishGridConverter()

    def addLatLng(row):
        row['lat'], row['lng'] = converter(row.ix[4], row.ix[5])
        return row

    csv_out = csv_file.apply(addLatLng, axis=1)
    csv_out.to_csv(csv_name + '-latlng.csv', index=False)


if __name__ == '__main__':
    run()
