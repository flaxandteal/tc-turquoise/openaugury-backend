All files in this directory are licensed under the UK Open Government License unless otherwise stated.

BusStops                            owner: Translink
LightingAssets                      owner: TransportNI
IndustrialHeritageRecord            owner: Department of Communities
AreasOfArchaeologicalPotential      owner: Department of Communities
ActivePlacesNI                      owner: Sport NI
DrainageAssets											owner: Department for Infrastructure
BelfastTrees												owner: Belfast City Council
TranslinkRailwayStations						owner: Translink
RegionalWatersSeeder								owner: Geological Survey of Northern Ireland
NICommunityCentre										owner: Belfast City Council
NILandfillSite											owner: Northern Ireland Environment Agency - Control & Data Management
NISchoolSeeder											owner: Department of Education
ListedBuildings											owner: Department for Communities - Historic Environment Division
