<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $caseFeatureChunk->id !!}</p>
</div>

<!-- Chunk Field -->
<div class="form-group">
    {!! Form::label('chunk', 'Chunk:') !!}
    <p>{!! $caseFeatureChunk->chunk !!}</p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    <p>{!! $caseFeatureChunk->case_context_id !!}</p>
</div>

<!-- Feature Set Id Field -->
<div class="form-group">
    {!! Form::label('feature_set_id', 'Feature Set Id:') !!}
    <p>{!! $caseFeatureChunk->feature_set_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $caseFeatureChunk->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $caseFeatureChunk->updated_at !!}</p>
</div>

