@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Case Feature Chunk') }}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'caseFeatureChunks.store']) !!}

                        @include('case_feature_chunks.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
