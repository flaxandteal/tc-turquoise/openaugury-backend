@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Analytic') }}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('analytics.show_fields')
                    <a href="{!! route('analytics.index') !!}" class="btn btn-default">{{ _i('Back') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
