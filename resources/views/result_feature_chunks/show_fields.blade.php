<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $resultFeatureChunk->id !!}</p>
</div>

<!-- Chunk Field -->
<div class="form-group">
    {!! Form::label('chunk', 'Chunk:') !!}
    <p>{!! $resultFeatureChunk->chunk !!}</p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    <p>{!! $resultFeatureChunk->simulation_id !!}</p>
</div>

<!-- Feature Set Id Field -->
<div class="form-group">
    {!! Form::label('feature_set_id', 'Feature Set Id:') !!}
    <p>{!! $resultFeatureChunk->feature_set_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $resultFeatureChunk->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $resultFeatureChunk->updated_at !!}</p>
</div>

