@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Cached Data Server Feature') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cachedDataServerFeature, ['route' => ['cachedDataServerFeatures.update', $cachedDataServerFeature->id], 'method' => 'patch']) !!}

                        @include('cached_data_server_features.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection