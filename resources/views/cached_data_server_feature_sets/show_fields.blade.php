<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cachedDataServerFeatureSet->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $cachedDataServerFeatureSet->name !!}</p>
</div>

<!-- Owner Field -->
<div class="form-group">
    {!! Form::label('owner', 'Owner:') !!}
    <p>{!! $cachedDataServerFeatureSet->owner !!}</p>
</div>

<!-- License Title Field -->
<div class="form-group">
    {!! Form::label('license_title', 'License Title:') !!}
    <p>{!! $cachedDataServerFeatureSet->license_title !!}</p>
</div>

<!-- License Url Field -->
<div class="form-group">
    {!! Form::label('license_url', 'License Url:') !!}
    <p>{!! $cachedDataServerFeatureSet->license_url !!}</p>
</div>

<!-- Uri Field -->
<div class="form-group">
    {!! Form::label('uri', 'Uri:') !!}
    <p>{!! $cachedDataServerFeatureSet->uri !!}</p>
</div>

<!-- Data Server Field -->
<div class="form-group">
    {!! Form::label('data_server', 'Data Server:') !!}
    <p>{!! $cachedDataServerFeatureSet->data_server !!}</p>
</div>

<!-- Data Server Set Id Field -->
<div class="form-group">
    {!! Form::label('data_server_set_id', 'Data Server Set Id:') !!}
    <p>{!! $cachedDataServerFeatureSet->data_server_set_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cachedDataServerFeatureSet->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cachedDataServerFeatureSet->updated_at !!}</p>
</div>

