@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Affiliation') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($affiliation, ['route' => ['affiliations.update', $affiliation->id], 'method' => 'patch']) !!}

                        @include('affiliations.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection