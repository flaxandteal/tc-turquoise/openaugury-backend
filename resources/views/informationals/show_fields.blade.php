<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $informational->id !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $informational->text !!}</p>
</div>

<!-- Subtext Field -->
<div class="form-group">
    {!! Form::label('subtext', 'Subtext:') !!}
    <p>{!! $informational->subtext !!}</p>
</div>

<!-- Informational Type Id Field -->
<div class="form-group">
    {!! Form::label('informational_type_id', 'Informational Type Id:') !!}
    <p>{!! $informational->informational_type_id !!}</p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    <p>{!! $informational->case_context_id !!}</p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    <p>{!! $informational->creator_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $informational->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $informational->updated_at !!}</p>
</div>

