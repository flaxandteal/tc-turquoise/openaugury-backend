<table class="table table-responsive" id="informationals-table">
    <thead>
        <th>{{ _i('Text') }}</th>
        <th>{{ _i('Subtext') }}</th>
        <th>{{ _i('Informational Type Id') }}</th>
        <th>{{ _i('Case Context Id') }}</th>
        <th>{{ _i('Creator Id') }}</th>
        <th colspan="3">{{ _i('Action') }}</th>
    </thead>
    <tbody>
    @foreach($informationals as $informational)
        <tr>
            <td>{!! $informational->text !!}</td>
            <td>{!! $informational->subtext !!}</td>
            <td>{!! $informational->informational_type_id !!}</td>
            <td>{!! $informational->case_context_id !!}</td>
            <td>{!! $informational->creator_id !!}</td>
            <td>
                {!! Form::open(['route' => ['informationals.destroy', $informational->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('informationals.show', [$informational->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('informationals.edit', [$informational->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>