<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtext Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('subtext', 'Subtext:') !!}
    {!! Form::textarea('subtext', null, ['class' => 'form-control']) !!}
</div>

<!-- Informational Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('informational_type_id', 'Informational Type Id:') !!}
    {!! Form::number('informational_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    {!! Form::text('case_context_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    {!! Form::text('creator_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('informationals.index') !!}" class="btn btn-default">Cancel</a>
</div>
