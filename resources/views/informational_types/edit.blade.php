@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Informational Type') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($informationalType, ['route' => ['informationalTypes.update', $informationalType->id], 'method' => 'patch']) !!}

                        @include('informational_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection