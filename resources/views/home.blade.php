@extends('layouts.app')

@section('title')
<div class="title row">
{{ _i('Our Chaotic World in Context') }}
</div>
<div class="subtitle row">
{{ _i('Bringing social, economic and cultural resilience to life') }}
</div>
@endsection

@section('content')
<div class="bg">
    <div class="splash bg big container-fluid" style="background-image: url('{{ config("openaugury.brand.splash.url-default") }}')">
        <div class="info container-fluid bg-container">
            <div class="row">
                <div class="col m-4">
                    @yield('title')
                </div>
                <div class="action col text-right m-4 mt-auto col-3">
                @guest
                    <a href="{{ route('login', app()->getLocale()) }}" class="btn btn-light btn-lg" type="button">{{ _i('View simulations') }}</a>
                @else
                    <a href="v/" class="btn btn-light btn-lg" type="button">{{ _i('View simulations') }}</a>
                @endguest
                </div>
            </div>
        </div>
    </div>
    <div class="splash bg small container-fluid" style="background-image: url('{{ config("openaugury.brand.splash.url-small") }}')">
        <div class="info">
            @yield('title')
            <div class="action row">
            @guest
                <a href="{{ route('login', app()->getLocale()) }}" class="btn btn-light btn-lg" type="button">{{ _i('View simulations') }}</a>
            @else
                <a href="v/" class="btn btn-light btn-lg" type="button">{{ _i('View simulations') }}</a>
            @endguest
            </div>
        </div>
    </div>
</div>
@endsection
