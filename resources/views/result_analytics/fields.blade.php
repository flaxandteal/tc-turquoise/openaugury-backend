<!-- Analytic Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('analytic_id', 'Analytic Id:') !!}
    {!! Form::number('analytic_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    {!! Form::text('simulation_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::textarea('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('resultAnalytics.index') !!}" class="btn btn-default">Cancel</a>
</div>
