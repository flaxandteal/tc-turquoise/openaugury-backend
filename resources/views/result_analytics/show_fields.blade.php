<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $resultAnalytic->id !!}</p>
</div>

<!-- Analytic Id Field -->
<div class="form-group">
    {!! Form::label('analytic_id', 'Analytic Id:') !!}
    <p>{!! $resultAnalytic->analytic_id !!}</p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    <p>{!! $resultAnalytic->simulation_id !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $resultAnalytic->value !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $resultAnalytic->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $resultAnalytic->updated_at !!}</p>
</div>

