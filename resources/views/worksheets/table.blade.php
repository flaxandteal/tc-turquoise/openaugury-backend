<table class="table table-responsive" id="worksheets-table">
    <thead>
        <th>{{ _i('Progress') }}</th>
        <th>{{ _i('Case Context Id') }}</th>
        <th>{{ _i('Worker Id') }}</th>
        <th colspan="3">{{ _i('Action') }}</th>
    </thead>
    <tbody>
    @foreach($worksheets as $worksheet)
        <tr>
            <td>{!! $worksheet->progress !!}</td>
            <td>{!! $worksheet->case_context_id !!}</td>
            <td>{!! $worksheet->worker_id !!}</td>
            <td>
                {!! Form::open(['route' => ['worksheets.destroy', $worksheet->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('worksheets.show', [$worksheet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('worksheets.edit', [$worksheet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>