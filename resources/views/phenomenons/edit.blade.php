@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Phenomenon') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($phenomenon, ['route' => ['phenomenons.update', $phenomenon->id], 'method' => 'patch']) !!}

                        @include('phenomenons.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection