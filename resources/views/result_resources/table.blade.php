<table class="table table-responsive" id="resultResources-table">
    <thead>
        <th>{{ _i('Resource Id') }}</th>
        <th>{{ _i('Simulation Id') }}</th>
        <th>{{ _i('Quantity') }}</th>
        <th>{{ _i('Duration') }}</th>
        <th colspan="3">{{ _i('Action') }}</th>
    </thead>
    <tbody>
    @foreach($resultResources as $resultResource)
        <tr>
            <td>{!! $resultResource->resource_id !!}</td>
            <td>{!! $resultResource->simulation_id !!}</td>
            <td>{!! $resultResource->quantity !!}</td>
            <td>{!! $resultResource->duration !!}</td>
            <td>
                {!! Form::open(['route' => ['resultResources.destroy', $resultResource->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('resultResources.show', [$resultResource->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('resultResources.edit', [$resultResource->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>