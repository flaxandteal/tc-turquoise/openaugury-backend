@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Challenge') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($challenge, ['route' => ['challenges.update', $challenge->id], 'method' => 'patch']) !!}

                        @include('challenges.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection