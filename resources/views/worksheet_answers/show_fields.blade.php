<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $worksheetAnswer->id !!}</p>
</div>

<!-- First Field -->
<div class="form-group">
    {!! Form::label('first', 'First:') !!}
    <p>{!! $worksheetAnswer->first !!}</p>
</div>

<!-- Final Field -->
<div class="form-group">
    {!! Form::label('final', 'Final:') !!}
    <p>{!! $worksheetAnswer->final !!}</p>
</div>

<!-- Mark Field -->
<div class="form-group">
    {!! Form::label('mark', 'Mark:') !!}
    <p>{!! $worksheetAnswer->mark !!}</p>
</div>

<!-- Worksheet Id Field -->
<div class="form-group">
    {!! Form::label('worksheet_id', 'Worksheet Id:') !!}
    <p>{!! $worksheetAnswer->worksheet_id !!}</p>
</div>

<!-- Challenge Id Field -->
<div class="form-group">
    {!! Form::label('challenge_id', 'Challenge Id:') !!}
    <p>{!! $worksheetAnswer->challenge_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $worksheetAnswer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $worksheetAnswer->updated_at !!}</p>
</div>

