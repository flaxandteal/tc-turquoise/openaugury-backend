<!-- First Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('first', 'First:') !!}
    {!! Form::textarea('first', null, ['class' => 'form-control']) !!}
</div>

<!-- Final Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('final', 'Final:') !!}
    {!! Form::textarea('final', null, ['class' => 'form-control']) !!}
</div>

<!-- Mark Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mark', 'Mark:') !!}
    {!! Form::number('mark', null, ['class' => 'form-control']) !!}
</div>

<!-- Worksheet Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('worksheet_id', 'Worksheet Id:') !!}
    {!! Form::text('worksheet_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Challenge Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('challenge_id', 'Challenge Id:') !!}
    {!! Form::text('challenge_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('worksheetAnswers.index') !!}" class="btn btn-default">Cancel</a>
</div>
