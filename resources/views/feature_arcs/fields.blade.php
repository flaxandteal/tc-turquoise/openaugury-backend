<!-- Feature Chunk Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('feature_chunk_id', 'Feature Chunk Id:') !!}
    {!! Form::number('feature_chunk_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    {!! Form::text('simulation_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Arc Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('arc', 'Arc:') !!}
    {!! Form::textarea('arc', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('featureArcs.index') !!}" class="btn btn-default">{{ _i('Cancel') }}</a>
</div>
