<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $featureArc->id !!}</p>
</div>

<!-- Feature Chunk Id Field -->
<div class="form-group">
    {!! Form::label('feature_chunk_id', 'Feature Chunk Id:') !!}
    <p>{!! $featureArc->feature_chunk_id !!}</p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    <p>{!! $featureArc->simulation_id !!}</p>
</div>

<!-- Arc Field -->
<div class="form-group">
    {!! Form::label('arc', 'Arc:') !!}
    <p>{!! $featureArc->arc !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $featureArc->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $featureArc->updated_at !!}</p>
</div>

