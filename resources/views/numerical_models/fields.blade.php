<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Definition Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('definition', 'Definition:') !!}
    {!! Form::textarea('definition', null, ['class' => 'form-control']) !!}
</div>

<!-- Phenomenon Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phenomenon_id', 'Phenomenon Id:') !!}
    {!! Form::number('phenomenon_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    {!! Form::text('owner_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('numericalModels.index') !!}" class="btn btn-default">Cancel</a>
</div>
