<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $numericalModel->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $numericalModel->name !!}</p>
</div>

<!-- Definition Field -->
<div class="form-group">
    {!! Form::label('definition', 'Definition:') !!}
    <p>{!! $numericalModel->definition !!}</p>
</div>

<!-- Phenomenon Id Field -->
<div class="form-group">
    {!! Form::label('phenomenon_id', 'Phenomenon Id:') !!}
    <p>{!! $numericalModel->phenomenon_id !!}</p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    <p>{!! $numericalModel->owner_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $numericalModel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $numericalModel->updated_at !!}</p>
</div>

