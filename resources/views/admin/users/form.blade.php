@extends('twill::layouts.form')

@push('extra_css')
<link rel="stylesheet" href="//unpkg.com/leaflet/dist/leaflet.css" />
@endpush

@push('extra_js_head')
<script src="//unpkg.com/leaflet/dist/leaflet.js"></script>
<script src="//unpkg.com/vue2-leaflet"></script>
@endpush

@section('contentFields')
    @formField('input', [
        'name' => 'email',
        'label' => 'Email',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'forename',
        'label' => 'Forename',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'surname',
        'label' => 'Surname',
        'maxlength' => 100
    ])

    @formField('select', [
        'name' => 'affiliation_id',
        'label' => 'Affiliation',
        'placeholder' => 'Select an affiliation',
        'options' => $affiliations,
        'maxlength' => 100
    ])
@stop
