

@formField('select', [
    'name' => 'simulation_id',
    'label' => 'Simulation',
    'placeholder' => 'Select a Simulation',
    'options' => $simulations
])

@formField('select', [
    'name' => 'challenge_type',
    'label' => 'Challenge Type',
    'placeholder' => 'Select a Challenge Type',
    'options' => $challengeTypes,
    'required' => true,
    
])

@formField('select', [
    'name' => 'challenge_level',
    'label' => 'Challenge Level',
    'placeholder' => 'Select a Challenge Level',
    'options' => $challengeLevels,
    'required' => true,
    
])

@formField('input', [
    'name' => 'text',
    'label' => 'Title',
    'placeholder' => 'Enter header text'
])


@formField('date_picker', [
'name' => 'time',
'label' => 'Time',
//'minDate' => $futureDateTime
//'start' => '2031-12-10 12:00'
])
