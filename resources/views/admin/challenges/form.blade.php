@extends('twill::layouts.form')

@push('extra_css')
  <link rel="stylesheet" href="//unpkg.com/leaflet/dist/leaflet.css" />
@endpush

@push('extra_js_head')
  <script src="//unpkg.com/leaflet/dist/leaflet.js"></script>
  <script src="//unpkg.com/vue2-leaflet"></script>
@endpush

@section('contentFields')
@formField('input', [
    'name' => 'url',
    'label' => 'Website URL',
    'placeholder' => 'http://www.website-project.com',
    'note' => 'Please enter full URL.',
    'maxlength' => 200
])

@formField('wysiwyg', [
    'name' => 'subtext',
    'label' => 'What do I need to do',
    'placeholder' => 'Enter question',
    'toolbarOptions' => [
      ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["direction" => "rtl"]],
      'required' => true,
])

@formField('files', [
    'name' => 'taskFile',
    'noTranslate' => true,
    'label' => 'File',
    'note' => 'Supported Formats; .pdf, .doc, .txt',
])

@if ($item->challenge_type === "multiple_choice")
@formField('input', [
    'name' => 'option0',
    'label' => 'Option 1',
    'maxlength' => 200,
    'placeholder' => 'Answer 1',
    'required' => true,
])
@formField('input', [
  'name' => 'option1',
  'label' => 'Option 2',
  'maxlength' => 200,
  'placeholder' => 'Answer 2',
  'required' => true,
])
@formField('input', [
  'name' => 'option2',
  'label' => 'Option 3',
  'maxlength' => 200,
  'placeholder' => 'Answer 3',
  'required' => true,
])
@formField('input', [
  'name' => 'option3',
  'label' => 'Option 4',
  'maxlength' => 200,
  'placeholder' => 'Answer 4',
  'required' => true,
])
@formField('select', [
  'name' => 'correct',
  'label' => 'Correct Answer',
  'placeholder' => 'Select an correct answer',
  'required' => true,
  'options' => [
      [
          'value' => 0,
          'label' => 'Answer 1'
      ],
      [
          'value' => 1,
          'label' => 'Answer 2'
      ],
      [
          'value' => 2,
          'label' => 'Answer 3'
      ],
      [
          'value' => 3,
          'label' => 'Answer 4'
      ]
  ]
])
@endif
@formField('hidden', [
  'name' => 'mark',
  'label' => 'Mark',
  'maxlength' => 200,
  'placeholder' => 'Question mark',
  'required' => true,
])
@stop

