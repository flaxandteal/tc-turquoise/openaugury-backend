@formField('select', [
    'name' => 'case_context_id',
    'label' => 'Target Zone',
    'placeholder' => 'Select a target zone',
    'options' => $caseContexts
])

@formField('select', [
    'name' => 'combination_id',
    'label' => 'Model',
    'placeholder' => 'Select a phenomenon and numerical model',
    'options' => $combinations
])

@formField('hidden', [
    'name' => 'settings',
    'value' => '[]'
])

@formField('hidden', [
    'name' => 'definition',
    'value' => '[]'
])

@formField('hidden', [
    'name' => 'status',
    'value' => 'STATUS_INACTIVE'
])

@formField('hidden', [
    'name' => 'result',
    'value' => '[]'
])
