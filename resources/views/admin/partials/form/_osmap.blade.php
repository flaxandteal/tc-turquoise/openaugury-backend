@php
/**
 * This file, unlike most of the code in this codebase, is under Apache 2.0 license.
 * https://www.apache.org/licenses/LICENSE-2.0
 * We gratefully acknowledge the work of the Area17/Twill team in creating
 * the original LocationField.vue
 */
    $showMap = $showMap ?? true;
    $openMap = $openMap ?? false;
    $inModal = $fieldsInModal ?? false;
    $initialLocation = $initialLocation ?? [0, 0];
    $initialZoom = $initialZoom ?? 9;
    $disabled = $disabled ?? false;
@endphp

<a17-oslocationfield
    label="{{ $label }}"
    @if ($disabled) disabled @endif
    @include('twill::partials.form.utils._field_name')
    @if ($inModal) :in-modal="true" @endif
    initialLat="{{ $initialLocation[0] }}"
    initialLng="{{ $initialLocation[1] }}"
    zoom="{{ $initialZoom }}"
    in-store="value"
></a17-oslocationfield>

@unless($renderForBlocks || $renderForModal || !isset($item->$name))
@push('vuexStore')
    window['{{ config('twill.js_namespace') }}'].STORE.form.fields.push({
        name: '{{ $name }}',
        value: {!! json_encode($item->$name) !!}
    })
@endpush
@endunless
