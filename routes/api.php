<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('getTaskFile/{challengeID}', 'ChallengeAPIController@getTaskFile');
Route::get('getNewsFile/{informationalID}', 'InformationalAPIController@getNewsFile');


Route::group(['middleware' => 'auth:api', 'prefix' => 'v1'], function () {
    Route::resource('users', 'UserAPIController');
    Route::resource('case-contexts', 'CaseContextAPIController');
    Route::resource('caseContexts', 'CaseContextAPIController');
    Route::resource('case-feature-chunks', 'CaseFeatureChunkAPIController');
    Route::resource('affiliations', 'AffiliationAPIController');
    Route::resource('districts', 'DistrictAPIController');
    Route::resource('phenomenons', 'PhenomenonAPIController');
    Route::resource('numericalModels', 'NumericalModelAPIController');

    Route::resource('combinations', 'CombinationAPIController');
    Route::resource('simulations', 'SimulationAPIController');
    Route::get('simulations/{id}/feature-arcs/{featureArcId}', 'SimulationAPIController@featureArcShow');
    Route::get('simulations/{id}/features', 'SimulationAPIController@featuresIndex');
    Route::get('simulations/{id}/statistics', 'SimulationAPIController@statisticsIndex');
    Route::get('simulations/{id}/features/{featureChunkId}', 'SimulationAPIController@featuresShow');
    Route::get('simulations/{id}/informationals', 'SimulationAPIController@informationalIndex');
    Route::get('simulations/{id}/challenges', 'SimulationAPIController@challengeIndex');
    Route::post('simulations/{id}/run', 'SimulationAPIController@run');
});

if (config('openaugury.anonymous-access', false)) {
    Route::group(['prefix' => 'v1'], function () {
        Route::get('case-contexts/{id}', 'CaseContextAPIController@show');
        Route::get('case-feature-chunks/{id}', 'CaseFeatureChunkAPIController@show');
        Route::get('affiliations/{id}', 'AffiliationAPIController@show');
        Route::get('phenomenons/{id}', 'PhenomenonAPIController@show');
        Route::get('numericalModels/{id}', 'NumericalModelAPIController@show');

        Route::get('simulations/{id}', 'SimulationAPIController@show');
        Route::get('simulations/{id}/feature-arcs/{featureArcId}', 'SimulationAPIController@featureArcShow');
        Route::get('simulations/{id}/features', 'SimulationAPIController@featuresIndex');
        Route::get('simulations/{id}/features/{featureChunkId}', 'SimulationAPIController@featuresShow');
        Route::get('simulations/{id}/informationals', 'SimulationAPIController@informationalIndex');
        Route::get('simulations/{id}/challenges', 'SimulationAPIController@challengeIndex');
    });
}
