<?php

Route::module('simulations');
Route::module('caseContexts');
Route::module('districts');
Route::module('challenges');
Route::module('informationals');
Route::module('affiliations');
Route::module('users');
Route::post('/logout', 'AuthController@logout')
    ->name('logout');
Route::get('/simulations/{simulation}/run', 'SimulationController@actionRun')
    ->name('simulations.run');
Route::get('/login')->name('login');
// Register Twill routes here (eg. Route::module('posts'))
