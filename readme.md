# openaugury-backend

> Back-end infrastructure for the OpenAugury project

# OpenAugury

**For important AGPL licensing information, please see the Licensing section below -
use of this codebase constitutes acceptance of license terms, and of the interpretation detailed
below.**

## Our Chaotic World in Context

### Introduction

How would the flash-floods of South America, the earthquakes of New Zealand, the wildfires of California or volcanic activity of the Pacific Rim affect Northern Ireland?

It is a web application, with custom tools and interactive maps that allow students to apply basic simulations to their area in the region, using open data to estimate the impact on familiar local landmarks.

For example, if an earthquake similar to those of 2010/11 in Canterbury, NZ had its epicentre below Carrickfergus or Limavady, the system would show names of heritage sites that may be permanently lost, local sports facilities rendered unusable through earthquake liquefaction, or suburbs likely to be worst affected. This aids students in understanding the human impact of such events, while making it clear that such events are constrained to the regions in which they occur.

### What's the Point?

Students gain an understanding of how the dynamic nature of the planet affects the human and natural world, in measurable terms to which they can easily relate.

They visually retain an understanding of natural disasters, the need for resilience and the impact on individuals in those contexts.

At higher levels, they will learn the physical basis and limitations of such events. Student task support and teacher monitoring of progress will be included, based on teacher input.

### How does it fit into the school setting?

In a classroom setting, students have set objectives to work through a range of scenarios contributing to age appropriate learning objects (discussed in further detail below).

The interface will be tailored to simplify use for both students and teachers, to enable quick set-up and minimize in-classroom preparation time.

As the resource will be accessible internally and externally, it may be included into both lesson plans and assignments.

The primary application of this resource is within the Northern Ireland Curriculum section "The World Around Us" at Key Stages 1 and 2 and several elements within the Geography syllabus at Key Stage 3.

The interface would be designed to be age appropriate primarily for Key Stage 2 and 3 students, with optional features that may be enabled for GCSE level.

### What's the Difference?

This approach is innovative in that it brings together several elements of the Earth Science syllabus that some students may find hard to relate to personally, with context-giving local data.

While online tools, such as the NI Rivers Agency flood map, demonstrate feasibility of producing a usable adverse condition web-based resource for NI,
this idea encompasses a number of environmental hazards, placing them in the same resource and workflow. Moreover, this resource will be tailored for classroom use from the ground up -
we have made provisions to include on-going professional teacher input and have incorporated feedback into our project outline based on secondary school teaching experience.

Where specific models are established, we use our own numerical simulation expertise to incorporate these - we are very keen for knowledgable individuals to contribute their own simulation modules!

Open data forms a critical part of the application, allowing users to better empathasize with the online simulation resource.
Moreover, this enables the entire data stream to be open and usable by schools without additional licensing costs and, while usable without further expansion or data,
can be enhanced as additional local information appears through OpenDataNI.

The software will be entirely open sourced, allowing schools not only to use it, but other developers around the world to submit material for inclusion, or adjust it
for their own use.

This project provides a simple API to be usable from student Python projects, so that they can download the tools and use them as software libraries in Raspberry Pi events
and extracurricular coding clubs.

This approach is also gains long-term sustainability through this innovative approach - by building a modular, extensible tool that students can learn to use programmatically,
enhancements can be cooperative and collaborative as future data becomes available, or new types of natural phenomena are incorporated.  However, the completed tool is able to
function completely without further development, as the required datasets are sufficient without further updating or future data release.

### What's the Open Data?

As an NI-funded project, we will incorporate a number of OpenDataNI datasets, in critical aspects of the application. Combined with the open source nature of the application,
additional open datasets may be incorporated into the application through developer extensions.

By providing the application as open source and supporting simple Python libraries, a pathway for interested teachers or students to using open data themselves is built from
classroom use to hobby or expansion use, enhancing both the educational and open attributes of the project, and raising the profile of open data applications.

Several datasets from organizations outside NI may be required, such as the Copernicus satellite data, which includes a range of useful data, both on terrain elevation and
on emergency mapping of a range of natural disasters. Data from the data.gov.uk portal relevant to Northern Ireland may also be used.

OpenDataNI datasets planned for inclusion:

 * Active Places NI - Sports Facilities Database
 * City Parks
 * Community Centres
 * Historic Parks and Gardens
 * Industrial Heritage Record
 * Library Locations in Northern Ireland
 * Lighting Assets
 * Listed Buildings Northern Ireland
 * Northern Ireland Railways Stations (and related datasets)
 * Playgrounds
 * Sports Pitches Playing Fields
 * Translink Bus Stop List
 * Translink Bus & Rail Stations

Datasets that may be also be used for derived data:

 * Northern Ireland Index of Production
 * Northern Ireland local authority collected municipal waste management
   statistics
 * Northern Ireland Water Bodies datasets
 * OSNI Open Data Townland Raster Maps (through data extraction)

### I'm not in NI!

For the moment, we are focusing on Northern Ireland - if you would be interested in funding or part-funding data analysis work for your local context (internally or through us), do get in touch.


# Licensing

This repository is AGPL - please note that this means that, unless agreed otherwise, reusing ANY code or serving all or part of this software to users is highly likely to mean you have to make your final app fully open source, including any simulation/backend functions (your additions can be under any AGPL-compatible open source license, from MIT to AGPL).

## What does this cover?

At present and by default, this covers any use of this software with your own simulation functions - *if you give access to anyone else, you are legally required to give them unresetricted access to any modifications and the combined work, _including any simulation function code_*. This also covers self-hosting any AGPL components for others, with or without a web interface.

*Of course, if you do not give access to anyone else, to this software or your combined work, you are not required to publicly release any changes* - but you must not put confidentiality or sharing restrictions (beyond standard open source license requirements) on anyone else, including on the simulation function code they have used via this software, which they have a right to access. This includes employees within the same organisation as well as third parties.

If you have ANY reservations about this, you MUST NOT use this software.

Please note that, at present, Flax &amp; Teal Limited is the sole holder of all AGPL code in this code-base and, as such, is entitled to agree separate commercial or charitable terms on a case-by-case basis. If you have a use-case that you feel is compatible with the open source principles of transparency but not the AGPL license used here, please get in touch.

## What if I want to contribute?

We do welcome contributions to the project - we do not believe Contributor License Agreements (CLAs) should be necessary for a range of reasons, but to ensure we are able to maintain our commercial work, we do require contributions to be under permissive licenses. Any merge requests to this repository will be taken to be under MIT license, unless stated otherwise. When merging, we effectively create a derived work based on your MIT-licensed changes under F&amp;T's AGPL license, but your MIT rights to your own code in the original merge request are unaffected. Acknowledgement and a link to your changes will be provided in our ACKNOWLEDGEMENTS file.

## But that's not fair!

While avoiding CLAs solves a number of key objections, one common concern that remains is that the primary copyright holder is allowed to sell commercial licenses to third-parties that contains contributed code (via merge requests), but other contributors cannot. This is still the case here - contributed code from anyone other than Flax &amp; Teal must be under permissive licenses, allowing us to sell the right to create non-open-source derivatives to our customers, while other contributors may only use or re-sell this application with all new code in the open.

If you are solely providing open source services to clients who are willing to consume fully open source services, these restrictions will not affect you.

If you wish to provide services, where your clients cannot see simulation function code, or other modifications, or contain closed-source freemium components, you will be in violation of the AGPL licensing agreement, unless you have obtained a license to do so from Flax &amp; Teal. Similarly, if your clients do so, they will also be in violation of the terms.

However, Flax &amp; Teal as a matter of company policy is not selling closed sourced or freemium content - we deliver open source software, and what you see here is what we sell. Any additional work that is not open is exclusively owned by and proprietary to our clients, even if commissioned from us - we are not selling any closed additions either.

## Doesn't open source mean all users should be unrestricted? AGPL restricts the projects other contributing developers...

We believe the objective of open source is to reduce proprietary restrictions to the ultimate users of a piece of software, and we believe this can necessitate incorporating open source restrictions, as here.

We usually license our libraries permissively (in contrast to this work) as the target users are developers. However, software such as this, where our target is non-technical end-users, uses strong licensing to ensure that our work drives the ecosystem to more transparency, and that end-users keep that freedom, at the cost of restrictions to proprietary development.

While Flax &amp; Teal does produce paid-for, client-owned proprietary code, we do so to make provide a net benefit to the open source ecosystem, (at least as long as our main commercial competitors are proprietary), by aiming for the best offering in the sector to be open source, by helping us avoid a freemium two-tier model, by funding non-volunteer requirements (such as security, compliance and penetration testing) and by ensuring essential project improvement timelines are not dependent on volunteer interest. Further, to avoid this becoming a net negative, we put contractual requirements in place to retain the open source focus of this project.

## I just want to try it out in the office and see if it works, doesn't this stop me?

To reiterate, if you are only providing this as a service to yourself, there are no relevant restrictions. If you want to get a few folks to try out your self-hosted version, and it hasn't materially changed from ours, then there's still no relevant restrictions. By that point, hopefully you will know if it's for you!

## I am a large enterprise that wants to wrap it up and offer it to my customers, how does this affect me?

Any changes you make will need to be released - however, more than that, whether you are running it live or batched, we explicitly consider the offering of an automated service that obtains results and returns them to a user to be a combined work. As an example, if a large cloud provider had this as a module that linked in, they would be required to open source at least parts of their user interface, their glue-code, their scheduling systems and any attendant infrastructure (so don't expect to see this on the big cloud providers, unless they've a commercial license).


## Will this change?

As this software gains momentum and the risk, investment and creative input ceases to be predominantly from Flax &amp; Teal, we plan to share our AGPL rights. We will do this by sharing our AGPL rights with a Contributors Trust, which will be able to decide which other contributors can create proprietary services or derived works and on what terms.

From an open source perspective, our ideal is that one day we will have a world where all software is unrestricted by secrecy or copyright to all users - however, in the current environment, this approach is the only realistic way to avoid building software that primarily benefits a proprietary competitor, thereby becoming a tool for restricting end-users' visibility into what their providers are doing. From a business perspective, ultimately sharing the rights in a Contributors Trust will allow the community to grow beyond one company, and ensures that the community can be financially sustainable independently of us - the only way an open source project, and so ourselves as open source providers, can succeed.

## I want to build or commission simulations or changes to this software

If you are happy for changes and simulation functions to be publicly shared, you are welcome to do so yourself, or to pay any other individual or organisation - you must make them aware of these terms, even if they are within your organisation.

If you are solely using providing access to yourself personally (commercially or non-commercially) - perhaps selling static results made using this software, or testing hypotheses locally - you are free to do so without further restriction.

If you are serving the software or parts of it to others, and not solely using the code for yourself (commercially or non-commercially), you must procure a non-AGPL license from Flax &amp; Teal Limited - this may or may not incur a cost.

## Does this affect the license of data created with this software?

No, as long as you do not share or serve the software (or portions of it) any data output is not considered part of a derived work - such as reports, static JSON created with our APIs, and so forth, is entirely your own.

# Note

YOUR USE OF THIS SOFTWARE OR RE-USE OF CODE HEREIN INDICATES LEGALLY-BINDING AGREEMENT WITH THE ABOVE TERMS.

For the remainder of the terms, please see the AGPLv3 license in the LICENSE file.

Any code not explicitly marked otherwise should be considered to be property of Flax &amp; Teal, licensed under the terms of the GNU AGPLv3 license. Certain other code is marked as under 3rd party licenses, which may not be AGPL.

# Based on Buckram

This project uses [Buckram](https://gitlab.com/flaxandteal/buckram) to provide re-usable docker-compose files
and Helm charts.

## A Laravel Chart

Caveat: this structure is still in active development.

### Laravel with GitlabCI+Kubernetes extensions

For details of the upstream Laravel project, see https://github.com/laravel/laravel

Using the `.gitlab-ci.yml` file in this directory, the necessary steps are run to build, test and deploy to an
AWS Elastic Container Repository defined in your Gitlab Variables:

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_ECR_URI
- AWS_SECRET_ACCESS_KEY

### Launching with Helm

You will require `kubectl`, a Kubernetes cluster configured and Docker installed (if building locally).

If using this source directory to build your images, make sure you run:
- `composer install`
before building. To build, run:
- `docker build -ti myimagename-nginx -f infrastructure/containers/nginx/Dockerfile .`
- `docker build -ti myimagename-phpfpm -f infrastructure/containers/phpfpm/Dockerfile .`
- `docker build -ti myimagename-artisan -f infrastructure/containers/artisan/Dockerfile .`
You will need these images to be available to your cluster from an accessible repository; the default demo
images built from this source are on Docker Hub. To use your own you will need to configure them in your `production.yaml` (see below).
In normal circumstances, your continuous delivery system will handle getting code to built images and on to
your cluster.

Start your Kubernetes cluster. If using minikube, then `minikube start`.

- If you have not used Helm on this cluster since it was created: `helm init`; if you have installed this Chart (Buckram Helm configuration) before, `helm list` to find the release name and `helm delete RELEASENAME`. Note that `helm init` will trigger a download of the Tiller pod (coordinates Helm actions on the cluster side), and until this has set itself up, Helm will not be ready for use.
- If you have not already, add an ingress controller: `helm install stable/traefik --namespace=kube-system --values traefik.yaml` (if you wish to have a Traefik dashboard, add `--set dashboard.enabled=true`)
- Copy `infrastructure/config/production.yaml.example` to `production.yaml` in a safe place and fill in the contents (e.g. APP_KEY)
- Make sure the hostname (where webpages should be served, not of the cluster) is correct in both places in `production.yaml`, and, if using minikube, that you have added it to your `/etc/hosts` file with the output of `minikube ip` as the address
- `helm dependencies update infrastructure/kubernetes/buckram`
- `helm install infrastructure/kubernetes/buckram --values PATH_TO_YOUR/production.yaml`

At present, the inter-dependencies are not ordered, so you may see, for instance, nginx starting before phpfpm and having to auto-restart before it gets the phpfpm server. Once `kubectl get pods` shows all pods running, your system should be good to go.

If using minikube, rather than a cloud provider, you will have to use a high numbered port to reach the ingress. Run `kubectl get svc --namespace=kube-system` and go to `HOSTNAME:PORT` in your browser, where `HOSTNAME` is the hostname used above, and `PORT` is the 3xxxx numbered port in the line with `traefik` in it (not `traefik-dashboard`)

To help web developers get familiar with Kubernetes, without trying to patch together multiple repositories,
the Helm chart for Kubernetes, container definitions and template secrets are under the `infrastructure`
subdirectory. However, bear in mind that, if you are experimenting
with the Kubernetes workflow by building and pushing images
it is recommended that you do so from a fresh clone of this repository so that
you do not include anything in the `.env` file (or elsewhere in the tree)
that should not end up in the test images. *In particular, make sure your `production.yaml`
is not in the tree*, especially if pushing experimentation images publicly.

(For those unfamiliar with Docker: in normal
workflow, the CI/CD train will clone your code repository
fresh and build the Docker images with your code inside,
then push to your chosen Docker image
repository, from where images may be deployed
to the cluster)

### Setting up

- `cd into frontend and run "git submodule update --init" to set up canvas layer`
- `go to the infrastructure folder in the backend and run 'git submodule update --init' to set up nginx correctly`
- `When setting up your DB you must first install PostGiS to run migrations`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan migrate`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=DatabaseSeeder`
To include sample data:
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=SampleUsersTableSeeder`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=OpenDataSeeder`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=SimulationsTableSeeder`

