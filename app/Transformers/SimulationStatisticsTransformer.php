<?php

namespace App\Transformers;

use App\Models\SimulationTier\Simulation;
use League\Fractal;

class SimulationStatisticsTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
    ];

    protected $defaultIncludes = [
    ];

    public function transform(Simulation $simulation)
    {
        $simulationArray = $simulation->toArray();
        $resultStatistics = $simulation->resultFeatureChunks()->summary()->get();

        return [
            'id' => $simulation->id,
            'timeOffsets' => $resultStatistics->reduce(function ($agg, $resultFeatureChunk) {
                $agg[$resultFeatureChunk['time_offset']] = $resultFeatureChunk['statistics'];
                return $agg;
            })
        ];
    }

    public function includeFeatureArcs(Simulation $simulation)
    {
        $featureArcs = $simulation->featureArcs;

        return $this->collection($featureArcs, new FeatureArcTransformer, 'featureArcs');
    }

    public function includeNumericalModel(Simulation $simulation)
    {
        $numericalModel = null;
        if ($simulation->combination) {
            $numericalModel = $simulation->combination->numericalModel;
        }

        if (! $numericalModel) {
            return null;
        }

        return $this->item($numericalModel, new NumericalModelTransformer, 'numericalModels');
    }

    public function includePhenomenon(Simulation $simulation)
    {
        $phenomenon = null;
        if ($simulation->combination && $simulation->combination->numericalModel) {
            $phenomenon = $simulation->combination->numericalModel->phenomenon;
        }

        if (! $phenomenon) {
            return null;
        }

        return $this->item($phenomenon, new PhenomenonTransformer, 'phenomenons');
    }

    public function includeCaseContext(Simulation $simulation)
    {
        $caseContext = $simulation->caseContext;

        if (! $caseContext) {
            return null;
        }

        return $this->item($caseContext, new CaseContextTransformer, 'caseContexts');
    }
}
