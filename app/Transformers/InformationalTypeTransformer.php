<?php

namespace App\Transformers;

use App\Models\Interaction\InformationalType;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="InformationalType",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="slug",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 * )
 */
class InformationalTypeTransformer extends Fractal\TransformerAbstract
{
    public function transform(InformationalType $informationalType)
    {
        $informationalTypeArray = $informationalType->toArray();

        return [
            'id' => $informationalTypeArray['slug'],
            'name' => $informationalTypeArray['name'],
        ];
    }
}
