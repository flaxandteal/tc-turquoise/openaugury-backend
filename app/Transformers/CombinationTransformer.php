<?php

namespace App\Transformers;

use App\Models\AbstractTier\Combination;
use League\Fractal;

class CombinationTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
            'numericalModel',
            'phenomenon'
    ];

    protected $defaultIncludes = [
    ];

    public function transform(Combination $combination)
    {
        $phenomenonSlug = null;
        if ($combination->numerical_model_id) {
            $numericalModel = $combination->numericalModel;
            if ($numericalModel->phenomenon_id) {
                $phenomenon = $numericalModel->phenomenon;
                $phenomenonSlug = $phenomenon->slug;
            }
        }

        $combinationArray = $combination->toArray();

        return [
            'id' => $combinationArray['id'],
            'status' => $combinationArray['status'],
            'owner_id' => $combinationArray['owner_id'],
            'phenomenon_id' => $phenomenonSlug,
            'created_at' => $combinationArray['created_at'],
            'updated_at' => $combinationArray['updated_at']
        ];
    }

    public function includeNumericalModel(Combination $combination)
    {
        $numericalModel = $combination->numericalModel;

        return $this->item($numericalModel, new NumericalModelTransformer, 'numericalModels');
    }

    public function includePhenomenon(Combination $combination)
    {
        $phenomenon = null;
        if ($combination->numericalModel) {
            $phenomenon = $combination->numericalModel->phenomenon;
        }

        if (! $phenomenon) {
            return null;
        }

        return $this->item($phenomenon, new PhenomenonTransformer, 'phenomenons');
    }
}
