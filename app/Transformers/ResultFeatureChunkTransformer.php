<?php

namespace App\Transformers;

use App\Models\SimulationTier\ResultFeatureChunk;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="ResultFeatureChunk",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="feature_set_id",
 *          description="feature_set_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="time_offset",
 *          description="time_offset",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="simulation_id",
 *          description="Simulation UUID",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="chunk",
 *          description="array of GeoJSON objects representing this chunk",
 *          type="array",
 *          format="array"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ResultFeatureChunkTransformer extends Fractal\TransformerAbstract
{
    public function transform(ResultFeatureChunk $resultFeatureChunk)
    {
        return [
            'id' => $resultFeatureChunk->id,
            'chunk' => json_decode($resultFeatureChunk->chunk, true),
            'time_offset' => $resultFeatureChunk->time_offset,
            'simulation_id' => $resultFeatureChunk->simulation_id,
            'feature_set_id' => $resultFeatureChunk->feature_set_id,
            'created_at' => $resultFeatureChunk->created_at,
            'updated_at' => $resultFeatureChunk->updated_at
        ];
    }
}
