<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\OrpFeatureHelper;
use App\Models\SimulationTier\Simulation;
use App\Models\CaseTier\CaseContext;
use App\Models\Interaction\Challenge;
use App\Models\Interaction\ChallengeTemplate;
use App\Models\Interaction\Informational;
use App\Models\Interaction\InformationalType;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Models\CaseTier\CaseFeatureChunk;
use App\Models\Features\FeatureSet;
use Illuminate\Routing\UrlGenerator;
use Auth;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(OrpFeatureHelper $featureHelper, UrlGenerator $urlGenerator)
    {
        if (!config('openaugury.frontend.allow-http')) {
            $urlGenerator->forceScheme('https');
        }

        Simulation::saving(function ($simulation) {
            if (Auth::user() && !Auth::user()->hasRole('admin')) {
                $simulation->owner_id = Auth::user()->id;
            }
        });

        Challenge::creating(function ($challenge) {
            $templateId = $challenge->challenge_template_id;
            if ($templateId) {
                $template = ChallengeTemplate::find($templateId);
                $challengeTemplateFields = [
                    'text',
                    'subtext',
                    'challenge_type',
                    'challenge_level',
                    'options',
                    'correct',
                    'mark'
                ];
                if ($template) {
                    foreach ($challengeTemplateFields as $field) {
                        if (!$challenge->getAttribute($field)) {
                            $challenge->setAttribute($field, $template->getAttribute($field));
                        }
                    }
                }
            }
        });

        Informational::creating(function ($informational) {
            $templateId = $informational->informational_type_id;
            if ($templateId) {
                $template = InformationalType::find($templateId);
                $informationalTypeFields = [
                    'text',
                    'subtext'
                ];
                if ($template) {
                    foreach ($informationalTypeFields as $field) {
                        if (!$informational->getAttribute($field)) {
                            $informational->setAttribute($field, $template->getAttribute($field));
                        }
                    }
                }
            }
        });

        CaseContext::saving(function ($caseContext) {
            if (Auth::user() && !Auth::user()->hasRole('admin')) {
                $caseContext->affiliation_id = Auth::user()->affiliation_id;
                $caseContext->owner_id = Auth::user()->id;
            }
        });

        CaseContext::saved(function ($caseContext) use ($featureHelper) {
            // At this point, we are in the middle of a performInsert,
            // so Phaza's PostgisTrait is currently storing the objects
            // in the geometries member, while the expressions are in the
            // respective attributes.
            //
            if ($caseContext->district && $caseContext->center && $caseContext->simulations()->count() === 0) {
                $originalCenter = $caseContext->getOriginal('center');

                /* Check whether the center has moved */
                if (!$originalCenter ||
                        abs($originalCenter->getLat() - $caseContext->center->getLat()) > 0.00001 ||
                        abs($originalCenter->getLng() - $caseContext->center->getLng()) > 0.00001) {

                    $caseContext->caseFeatureChunks()->delete();

                    $featureSets = $featureHelper->pull(
                        $caseContext->district,
                        $caseContext->center,
                        5000,
                        Auth::user()
                    );

                    $featureSets->each(function ($featureSet) use ($caseContext) {
                        $stats = json_encode([
                            'size' => $featureSet['features']->count()
                        ]);
                        $chunk = CaseFeatureChunk::create([
                            'case_context_id' => $caseContext->id,
                            'feature_set_id' => $featureSet['featureSet']->id,
                            'status' => $featureSet['status'],
                            'stats' => $stats,
                            'chunk' => json_encode($featureSet['features']->map(function ($f) {
                                return [
                                    'feature_id' => $f->feature_id,
                                    'location' => $f->location,
                                    'extent' => $f->extent,
                                    'data' => json_decode($f->json)
                                ];
                            })->toArray())
                        ]);
                    });

                    $focalPoint = FeatureSet::whereSlug('focal-point')->first();
                    if ($focalPoint && isset($caseContext->center)) {
                        CaseFeatureChunk::create([
                            'case_context_id' => $caseContext->id,
                            'feature_set_id' => $focalPoint->id,
                            'status' => CaseFeatureChunk::STATUS_ACTIVE,
                            'chunk' => json_encode([
                                [
                                    'feature_id' => 1,
                                    'location' => $caseContext->center
                                ]
                            ])
                        ]);
                    }
                }
            }
        });

        \App\Models\SimulationTier\Simulation::saving(function ($simulation) {
            if (!$simulation->begins && !$simulation->ends) {
                $simulation->updateFromCase();
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
