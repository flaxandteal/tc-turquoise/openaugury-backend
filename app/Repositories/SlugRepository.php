<?php

namespace App\Repositories;

use A17\Twill\Repositories\ModuleRepository;

abstract class SlugRepository extends ModuleRepository
{
    /**
     * Find an item by its slug
     **/
    public function findBySlug($slug, $columns = ['*'])
    {
        return $this->findByField('slug', $slug, $columns)->first();
    }
}
