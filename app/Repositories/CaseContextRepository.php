<?php

namespace App\Repositories;

use App\Models\CaseTier\CaseContext;
use App\Models\CaseTier\CaseFeatureChunk;
use A17\Twill\Repositories\ModuleRepository;

class CaseContextRepository extends ModuleRepository
{

    public function __construct(CaseContext $model)
    {
        $this->model = $model;
    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'district_id',
        'name',
        'center',
        'extent',
        'begins',
        'ends',
        'owner_id'
    ];

    public function prepareFieldsBeforeSave($object, $fields) {
        unset($fields['center']);
        return parent:: prepareFieldsBeforeSave($object, $fields);
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CaseContext::class;
    }

    public function afterSave($object, $fields) {
        if (array_key_exists('activeFeatureChunks', $fields) && $fields['activeFeatureChunks'] === null) {
            $fields['activeFeatureChunks'] = [];
        }

        if (isset($fields['activeFeatureChunks'])) {
            $object->caseFeatureChunks()
                   ->whereIn('id', $fields['activeFeatureChunks'])
                   ->update([
                       'status' => CaseFeatureChunk::STATUS_ACTIVE
                   ]);
            $object->caseFeatureChunks()
                   ->whereNotIn('id', $fields['activeFeatureChunks'])
                   ->update([
                       'status' => CaseFeatureChunk::STATUS_INACTIVE
                   ]);
            unset($fields['activeFeatureChunks']);
        }

        parent::afterSave($object, $fields);
    }

}
