<?php

namespace App\Repositories;

use App\Models\AbstractTier\Phenomenon;
use App\Models\Users\Affiliation;

class PhenomenonRepository extends SlugRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'symbol',
        'color'
    ];

    /**
     * Configure the Model
     **/
    public function __construct(Phenomenon $model)
    {
        $this->model = $model;
    }
}
