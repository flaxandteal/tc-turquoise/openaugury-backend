<?php

namespace App\Repositories;

use App\Models\Features\FeatureSet;
use A17\Twill\Repositories\ModuleRepository;

class FeatureSetRepository extends ModuleRepository
{

    public function __construct(FeatureSet $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'description'
    ];

    /**
     * @param string $column
     * @param array $orders
     * @param null $exceptId
     * @return \Illuminate\Support\Collection
     */
    public function listSome($scopes = [], $column = 'title', $orders = [], $exceptId = null)
    {
        $query = $this->model->newQuery();

        $query = $this->filter($query, $scopes);

        if ($exceptId) {
            $query = $query->where($this->model->getTable() . '.id', '<>', $exceptId);
        }

        if ($this->model instanceof Sortable) {
            $query = $query->ordered();
        } elseif (!empty($orders)) {
            $query = $this->order($query, $orders);
        }

        if ($this->model->isTranslatable()) {
            $query = $query->withTranslation();
        }

        return $query->get()->pluck($column, 'id');
    }
}
