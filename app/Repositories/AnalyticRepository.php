<?php

namespace App\Repositories;

use App\Models\AbstractTier\Analytic;
use InfyOm\Generator\Common\BaseRepository;

class AnalyticRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Analytic::class;
    }
}
