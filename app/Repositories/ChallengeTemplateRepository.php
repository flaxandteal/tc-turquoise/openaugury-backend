<?php

namespace App\Repositories;

use App\Models\Interaction\ChallengeTemplate;
use InfyOm\Generator\Common\BaseRepository;

class ChallengeTemplateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'text',
        'subtext',
        'options',
        'correct',
        'mark',
        'global',
        'analytic_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ChallengeTemplate::class;
    }
}
