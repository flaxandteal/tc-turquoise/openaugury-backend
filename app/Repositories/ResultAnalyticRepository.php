<?php

namespace App\Repositories;

use App\Models\SimulationTier\ResultAnalytic;
use InfyOm\Generator\Common\BaseRepository;

class ResultAnalyticRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'analytic_id',
        'simulation_id',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResultAnalytic::class;
    }
}
