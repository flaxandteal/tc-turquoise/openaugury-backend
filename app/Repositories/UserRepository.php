<?php

namespace App\Repositories;

use App\Models\Users\User;
use A17\Twill\Repositories\ModuleRepository;

class UserRepository extends ModuleRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identifier',
        'password',
        'affiliation_id',
        'teacher_id',
        'forename',
        'surname',
        'email',
        'remember_token'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function __construct(User $model)
    {
        $this->model = $model;
    }
}
