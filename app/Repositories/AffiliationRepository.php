<?php

namespace App\Repositories;

use App\Models\Users\Affiliation;
use App\Models\AbstractTier\Combination;
use A17\Twill\Repositories\ModuleRepository;

class AffiliationRepository extends ModuleRepository
{
    public function __construct(Affiliation $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'district_id',
        'location'
    ];

    public function prepareFieldsBeforeSave($object, $fields) {
        unset($fields['location']);
        return parent:: prepareFieldsBeforeSave($object, $fields);
    }

    /**
     * Create a metered version of the model
     */
    public function createMetered($attributes)
    {
        $model = self::create($attributes);
        $model->setMetered(0);
        $model->save();
        return $model;
    }

    public function afterSave($object, $fields) {
        if (array_key_exists('activeCombinations', $fields) && $fields['activeCombinations'] === null) {
            $fields['activeCombinations'] = [];
        }

        if (isset($fields['activeCombinations'])) {
            $activeCombinationsPivot = [];
            foreach ($fields['activeCombinations'] as $combinationId) {
                $activeCombinationsPivot[$combinationId] = [
                    'status' => Combination::STATUS_ACTIVE
                ];
            }

            $object->combinations()->sync(
                $activeCombinationsPivot
            ); // Should have a way of linking/unlinking, as well as status
            unset($fields['activeCombinations']);
        }

        parent::afterSave($object, $fields);
    }
}
