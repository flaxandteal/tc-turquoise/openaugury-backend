<?php

namespace App\Repositories;

use App\Models\Interaction\Worksheet;
use InfyOm\Generator\Common\BaseRepository;

class WorksheetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'progress',
        'simulation_id',
        'worker_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Worksheet::class;
    }
}
