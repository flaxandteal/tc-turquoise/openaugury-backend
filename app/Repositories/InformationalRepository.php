<?php

namespace App\Repositories;

use App\Models\Interaction\Informational;
use A17\Twill\Repositories\ModuleRepository;
use A17\Twill\Repositories\Behaviors\HandleFiles;


class InformationalRepository extends ModuleRepository
{
    use HandleFiles;
    
    public function __construct(Informational $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'url',
        'subtext',
        'informational_type_id',
        'simulation_id',
        'creator_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Informational::class;
    }
}
