<?php

namespace App\Repositories;

use App\Models\CachedDataServerFeatureSet;
use InfyOm\Generator\Common\BaseRepository;

class CachedDataServerFeatureSetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'owner',
        'license_title',
        'license_url',
        'uri',
        'data_server',
        'data_server_set_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CachedDataServerFeatureSet::class;
    }
}
