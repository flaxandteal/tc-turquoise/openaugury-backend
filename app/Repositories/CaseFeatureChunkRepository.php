<?php

namespace App\Repositories;

use App\Models\CaseTier\CaseFeatureChunk;
use InfyOm\Generator\Common\BaseRepository;

class CaseFeatureChunkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'chunk',
        'case_context_id',
        'feature_set_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CaseFeatureChunk::class;
    }
}
