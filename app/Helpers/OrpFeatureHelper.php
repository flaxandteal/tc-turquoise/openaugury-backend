<?php namespace App\Helpers;

use Illuminate\Support\Collection;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;

class OrpFeatureHelper
{
    public function pull($district, $center, $radius, $user) {
        $dataSetFeatures = new Collection;

        $district->featureSets->each(function ($featureSet) use (&$dataSetFeatures, $center, $radius, $user) {
            if ($featureSet->slug === 'my-school') {
                // This process should be made more flexible, for affiliation specific datasets
                if ($user && $user->affiliation && $user->affiliation->location) {
                    $dataSetFeatures[] = [
                        'featureSet' => $featureSet,
                        'status' => $featureSet->pivot->status,
                        'features' => collect([new CachedDataServerFeature([
                            'name' => 'My School',
                            'feature_id' => 'aff-' . $user->affiliation->id,
                            'location' => $user->affiliation->location,
                            'extent' => null,
                            'data' => [
                                'title' => $user->affiliation->name,
                                'description' => 'Your institution',
                                'affiliationId' => $user->affiliation->id
                            ]
                        ])])
                    ];
                }
            } else {
                $cachedDataSet = CachedDataServerFeatureSet::whereDataServerSetId($featureSet->pivot->data_server_set_id)->whereDataServer($featureSet->pivot->data_server)->first();
                if ($cachedDataSet) {
                    $features = $cachedDataSet
                        ->cachedDataServerFeatures()
                        ->whereRaw("ST_DWithin(location, ST_GeomFromText('" . $center->toWKT() . "'), " . $radius . ")");

                    if ($features->count()) {
                        $dataSetFeatures[] = [
                            'featureSet' => $featureSet,
                            'status' => $featureSet->pivot->status,
                            'features' => $features->get()
                        ];
                    }
                }
            }
        });

        return $dataSetFeatures;
    }
}
