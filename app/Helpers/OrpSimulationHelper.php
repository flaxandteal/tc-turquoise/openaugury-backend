<?php namespace App\Helpers;

use Config;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\RequestOptions;

// This class is temporary until an external job scheduler is incorporated
class OrpSimulationHelper
{
    protected $client;

    protected $expiration;

    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
        $this->host = config('orp.host');
        $this->expiration = Config::get('openaugury.simulation.expiration', 600);

        $memory = Config::get('openaugury.simulation.maximum_decode_memory');
        if ($memory) {
            ini_set('memory_limit', $memory);
        }
    }

    public function persistPortfolio($id, $settings, $features)
    {
        $expiration = $this->expiration;

        $key = 'orp_simulation_' . $id . '_query';
        // $this->redis->set($key, $features);
        // $this->redis->expire($key, $expiration);

        \Storage::disk('minio')->put('simulations/' . $key . '.json', $features);

        $query = $settings;
        $query['minio_key'] = $key;

        return $query;
    }

    public function result($id, $begin, $end, $window, $center, $featureStates, $phenomenon, $model, $districtSlug)
    {
        $features = json_encode($featureStates);
        $settings = [
            'id' => $id,
            'version' => '2',
            'phenomenon' => $phenomenon,
            'definition' => $model,
            'model' => $model,
            'begin' => $begin,
            'end' => $end,
            'center' => $center,
            'window' => $window
        ];
        $query = $this->persistPortfolio($id, $settings, $features);

        // This approach is temporary and will be replaced with a proper job queue asap (as is used elsewhere)
        $server = Config::get('openaugury.simulation.server');
        $port = Config::get('openaugury.simulation.port');

        $functionPrefix = Config::get('openaugury.simulation.function-prefix', 'orp-core-model');

        //\Storage::disk('minio')->put('testing/1', 'TESTING');

        // FIXME: access control for functions
        $model = json_decode($model, true);
        if (array_key_exists('type', $model)) {
            $functionSuffix = $model['type'];
        } else {
            $functionSuffix = $phenomenon;
        }

        if ($districtSlug) {
            $districtSuffix = '-' . $districtSlug;
        } else {
            $districtSuffix = '';
        }

        try {
            $response = $this->client->post($server . ':' . $port . '/function/' . $functionPrefix . '-' . $functionSuffix . $districtSuffix, [RequestOptions::JSON => $query]);
        } catch (ServerErrorResponseException $e) {
            \Log::info("Error from the simulation server...");
            \Log::info($e->getResponse()->getBody());
            return false;
        }

        $keys = json_decode($response->getBody(), true);
        $results = $this->retrieveResults($keys);

        return $results;
    }

    public function retrieveResults($keys)
    {
        $results = json_decode(\Storage::disk('minio')->get('simulations/' . $keys['results'] . '.json'), true);
        $featureArcs = json_decode(\Storage::disk('minio')->get('simulations/' . $keys['feature_arcs'] . '.json'), true);
        \Storage::disk('minio')->delete($keys['results']);
        \Storage::disk('minio')->delete($keys['feature_arcs']);

        return [
            'results' => $results,
            'feature_arcs' => $featureArcs
        ];
    }
}
