<?php

namespace App\Http\Controllers\Interaction;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

use Auth;
use App\Models\Interaction\Challenge;
use App\Models\Interaction\ChallengeTemplate;
use App\Models\SimulationTier\Simulation;
use App\Jobs\ChallengeRun;

class ChallengeController extends CrudController
{

    public function __construct(\Lang $lang, Request $request)
    {
        parent::__construct($lang);

        $this->request = $request;
        $this->features['all']['import-button'] = false;
        $this->features['all']['export-button'] = false;
    }

    public function all($entity)
    {
        parent::all($entity);
        
        $challengeModel = new Challenge;
        $challengeModel->with('simulation');

        if (!Auth::user()->hasRole('admin')) {
            $challengeModel = $challengeModel->whereHas('simulation', function ($q) {
                $q->whereOwnerId(Auth::user()->id);
            });
        }

        $add_url = null;
        if ($this->request->has('simulation_id')) {
            $simulationId = $this->request->get('simulation_id');
            $add_url = url('panel', $entity . '/edit') . '?step=1&simulation_id=' . $this->request->get('simulation_id');
            $challengeModel = $challengeModel->whereSimulationId($simulationId);

            $this->features['all']['return-button'] = [
                'target' => url('panel', 'SimulationTier:Simulation/edit') . '?modify=' . $simulationId,
                'label' => 'Return to Simulation'
            ];
        }

        $this->filter = \DataFilter::source($challengeModel);

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add_url = $add_url;
        $this->grid->add('{{ $time_offset > 0 ? "+" : "" }}{{ round($time_offset / 3600) }}h {{ abs(round($time_offset / 60) % 60) }}m', 'Time');
        $this->grid->add('text', 'Title');
        $this->grid->add('challenge_type', 'Challenge Type');
        //$this->grid->add('subtext', 'Content');
        $this->grid->add('time', 'Event Time');

        $this->addStylesToGrid();

        $this->grid->orderBy('time_offset');

        return $this->returnView('panelViews::all', ['title' => 'Tasks']);
    }

    public function edit($entity) {

        parent::edit($entity);

        $this->edit = \DataEdit::source(new Challenge);

        if ($this->request->has('simulation_id')) {
            $simulationId = $this->request->get('simulation_id');
        } else {
            if (!$this->edit->model || !$this->edit->model->simulation_id) {
                // TODO: eliminate or implement this route
                abort(404);
            } else {
                $simulationId = $this->edit->model->simulation_id;
            }
        }

        $simulation = Simulation::findOrFail($simulationId);

        $this->edit->label('Edit Task');

        if (!$this->edit->model->id) {
            $this->edit->add('simulation_id', 'Simulation', 'hidden')->insertValue($simulation->id);
            $this->edit->add('time_offset', 'Time offset', 'hidden')->insertValue(0);
            $this->edit->add('challenge_template_id', 'Template', 'select')
                ->options(ChallengeTemplate::pluck('text', 'id'));
        } else {
            $this->edit->add('simulation_id', 'Simulation', 'hidden')->insertValue($simulation->id);
            $this->edit->add('text', 'Title', 'text');
            $this->edit->add('challenge_type', 'ChallengeType', 'text');
            $this->edit->add('challenge_level', 'ChallengeLevel', 'text');
            $this->edit->add('author', 'Author to Display', 'text');
            $this->edit->add('time_offset', 'Seconds after simulation start<br/>(Start is ' . $simulation->begins . ')', 'text')->rule('integer');
            $this->edit->add('subtext', 'Content', 'textarea');
            $this->edit->add('mark', 'Mark', 'hidden')->insertValue(1);

            if ($this->edit->model->challenge_type == 'multiple_choice') {
                $this->edit->add('option0', 'First Option', 'text');
                $this->edit->add('option1', 'Second Option', 'text');
                $this->edit->add('option2', 'Third Option', 'text');
                $this->edit->add('option3', 'Four Option', 'text');
                $this->edit->add('correct', 'Correct', 'select')
                    ->options(['"a"' => 'a', '"b"' => 'b', '"c"' => 'c', '"d"' => 'd']);
            }
        }

        $this->edit->saved(function ($edit) use ($entity, $simulation) {
            if ($this->request->get('step') == '1') {
                return redirect(url('panel', $entity . '/edit') . '?step=2&modify=' . $edit->model->id);
            } else {
                return redirect(url('panel', $entity . '/all') . '?simulation_id=' . $simulation->id);
            }
        });

        return $this->returnEditView('panelViews::edit', ['title' => 'Edit Task']);
    }
}
