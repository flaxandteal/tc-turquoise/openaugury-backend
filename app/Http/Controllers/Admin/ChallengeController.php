<?php

namespace App\Http\Controllers\Admin;

use Auth;
use A17\Twill\Http\Controllers\Admin\ModuleController;
use Carbon\Carbon;

class ChallengeController extends ModuleController
{
    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => true,
        'bulkPublish' => true,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'forceDelete' => true,
        'bulkForceDelete' => true,
        'delete' => true,
        'duplicate' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false, //false to hide this in Add New
        'bulkEdit' => true,
        'editInModal' => false,
    ];

    protected $moduleName = 'challenges';

    protected $titleColumnKey = 'text';

    protected $perPage = 15;

    protected $indexColumns = [
        'text' => [ // field column
            'title' => 'Text',
            'field' => 'text',
        ],
        'subtext' => [ // field column
            'title' => 'Subtext',
            'field' => 'subtext',
            'visible' => false,
        ],
        'url' => [ // field column
            'title' => 'URL',
            'field' => 'url',
        ],
        'challenge_level' => [ // field column
            'title' => 'Challenge level',
            'field' => 'challenge_level',
        ],
        'challenge_type' => [ // field column
            'title' => 'Type',
            'field' => 'formatted_challenge_type',
        ],
        'time' => [ // field column
            'title' => 'Time',
            'field' => 'time',
        ],
        'options' => [
            'title' => 'Options',
            'field' => 'formatted_options',
            //'present' => true
        ],
        'correct' => [
            'title' => 'Correct',
            'field' => 'correct',
            //'present' => true
        ],
        /*'mark' => [ // field column
            'title' => 'Mark',
            'field' => 'mark',
        ],*/
        'created' => [ // field column
            'title' => 'Created',
            'field' => 'formatted_created_at',
        ],
        /*'updated' => [ // field column
            'title' => 'Updated',
            'field' => 'updated_at',
        ]*/
    ];

    protected function formData($request)
    {
        $simulations = app(\App\Repositories\SimulationRepository::class)->listAll('generated_title');
        $challengeTemplates = \App\Models\Interaction\ChallengeTemplate::get()->pluck('text', 'id');
        $challengeTypes = [
            ['value' => 'writtenWork', 'label' => 'Activity'],
            ['value' => 'multiple_choice', 'label' => 'Multiple choice' ]
        ];
        
        $challengeLevels = [
            ['value' => 'level1', 'label' => 'Level 1'],
            ['value' => 'level2', 'label' => 'Level 2' ]
        ];
        $futureDateTime = Carbon::now()->addYears(10);

        return [
            'simulations' => $simulations,
            'challengeTypes' => $challengeTypes,
            'challengeTemplates' => $challengeTemplates,
            'challengeLevels' => $challengeLevels,
            'futureDateTime' => $futureDateTime
        ];
    }

    protected function indexData($request)
    {
        $simulations = app(\App\Repositories\SimulationRepository::class)->listAll('generated_title');
        $challengeTemplates = \App\Models\Interaction\ChallengeTemplate::get()->pluck('text', 'id');
        $challengeTypes = [
            ['value' => 'writtenWork', 'label' => 'Activity'],
            ['value' => 'multiple_choice', 'label' => 'Multiple choice' ]
        ];

        $challengeLevels = [
            ['value' => 'level1', 'label' => 'Level 1'],
            ['value' => 'level2', 'label' => 'Level 2' ]
        ];

        $futureDateTime = Carbon::now()->addYears(10);

        return [
            'simulations' => $simulations,
            'challengeTypes' => $challengeTypes,
            'challengeTemplates' => $challengeTemplates,
            'challengeLevels' => $challengeLevels,
            'futureDateTime' => $futureDateTime
        ];
    }
}
