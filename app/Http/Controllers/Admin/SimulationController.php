<?php

namespace App\Http\Controllers\Admin;

use Auth;
use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Jobs\SimulationRun;

class SimulationController extends ModuleController
{
    protected $indexOptions = [
        'create' => true,
        'edit' => false,
        'publish' => true,
        'bulkPublish' => true,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'forceDelete' => true,
        'bulkForceDelete' => true,
        'delete' => true,
        'duplicate' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false, //false to hide this in Add New
        'bulkEdit' => true,
        'editInModal' => false,
    ];

    protected $moduleName = 'simulations';

    protected $titleColumnKey = 'generated_title';

    protected $indexColumns = [
        'generated_title' => [ // field column
            'title' => 'Title',
            'field' => 'generated_title',
        ],
        'target_zone' => [ // field column
            'title' => 'Target Zone',
            'field' => 'presenter_target_zone_link',
        ],
        'presenterStatusInteractive' => [
            'title' => 'Status',
            'field' => 'presenterStatusInteractive',
            //'present' => true
        ],
        'view' => [
            'title' => 'View',
            'field' => 'presenterView',
            //'present' => true
        ],
        'from' => [ // field column
            'title' => 'From',
            'field' => 'begins',
        ],
        'to' => [ // field column
            'title' => 'To',
            'field' => 'ends',
        ],
        'created' => [ // field column
            'title' => 'Created',
            'field' => 'created_at',
        ],
        'updated' => [ // field column
            'title' => 'Updated',
            'field' => 'updated_at',
        ],
        'status' => [ // field column
            'title' => 'Status',
            'field' => 'status',
        ],
    ];

    protected function formData($request)
    {
        $caseContexts = app(\App\Repositories\CaseContextRepository::class)->listAll('name');
        $combinations = \App\Models\AbstractTier\Combination::get()->pluck('phenomenonName', 'id');

        return [
            'caseContexts' => $caseContexts,
            'combinations' => $combinations
        ];
    }

    protected function indexData($request)
    {
        $caseContexts = app(\App\Repositories\CaseContextRepository::class)->listAll('name');
        $combinations = \App\Models\AbstractTier\Combination::get()->pluck('phenomenonName', 'id');

        return [
            'caseContexts' => $caseContexts,
            'combinations' => $combinations
        ];
    }


    public function actionRun($simulationId)
    {
        // TODO
        $simulation = app(\App\Repositories\SimulationRepository::class)->getById($simulationId);
        if ($simulation) {
            dispatch(new SimulationRun($simulation, Auth::user(), True));
            return redirect(route('admin.simulations.index', ['simulation' => $simulationId]));
        }
        abort(404, __("Simulation not found"));
    }
}
