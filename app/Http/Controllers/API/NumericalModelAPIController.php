<?php

namespace App\Http\Controllers\API;
use Log;
use Swis\JsonApi\Client\Parsers\DocumentParser;
use App\Http\Requests\API\CreateNumericalModelAPIRequest;
use App\Http\Requests\API\UpdateNumericalModelAPIRequest;
use App\Models\AbstractTier\NumericalModel;
use App\Repositories\NumericalModelRepository;
use App\Repositories\PhenomenonRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Transformers\NumericalModelTransformer;
use App\Transformers\ResultFeatureChunkTransformer;
use App\Transformers\InformationalTransformer;
use App\Transformers\ChallengeTransformer;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Auth;
use Spatie\Fractal\Fractal;
use App\Jobs\NumericalModelRun;

/**
 * Class NumericalModelController
 * @package App\Http\Controllers\API
 */

class NumericalModelAPIController extends AppBaseController
{
    /** @var  NumericalModelRepository */
    private $numericalModelRepository;

    /** @var  PhenomenonRepository */
    private $phenomenonRepository;

    public function __construct(NumericalModelRepository $numericalModelRepo, PhenomenonRepository $phenomenonRepo)
    {
        $this->numericalModelRepository = $numericalModelRepo;
        $this->phenomenonRepository = $phenomenonRepo;
        $this->auth = app()->make('auth');
    }

    public function index(Request $request)
    {
        // $this->numericalModelRepository->pushCriteria(new RequestCriteria($request));
        // $this->numericalModelRepository->pushCriteria(new LimitOffsetCriteria($request));
        $numericalModels = $this->numericalModelRepository->all();

        return fractal($numericalModels, new NumericalModelTransformer)
                ->withResourceName('numericalModels')->respond();
    }

    public function store(CreateNumericalModelAPIRequest $request, DocumentParser $parser)
    {
        $input = $request->all();
        $input['data']['id'] = '[none]';
        $document = $parser->parse(json_encode($input));

        $data = $document->getData();
        $phenomenon = $data->getRelations()['phenomenon']->getIncluded();
        $input = $data->toArray();
        $input['phenomenon_id'] = $this->phenomenonRepository->findBySlug($phenomenon->id)->id;

        $numericalModel = $this->numericalModelRepository->create($input);

        return fractal($numericalModel, new NumericalModelTransformer)
                ->withResourceName('numericalModels')->respond();
    }

    public function show($id)
    {
        /** @var NumericalModel $numericalModel */
        $numericalModel = $this->numericalModelRepository->find($id);

        if (empty($numericalModel)) {
            return $this->sendError('NumericalModel not found');
        }

        return fractal($numericalModel, new NumericalModelTransformer)
                ->withResourceName('numericalModels')->respond();
    }

    public function update($id, UpdateNumericalModelAPIRequest $request)
    {
        $input = $request->all();

        /** @var NumericalModel $numericalModel */
        $numericalModel = $this->numericalModelRepository->find($id);

        if (empty($numericalModel)) {
            return $this->sendError('NumericalModel not found');
        }

        $numericalModel = $this->numericalModelRepository->update($input, $id);

        return fractal($numericalModel, new NumericalModelTransformer)
                ->withResourceName('numericalModels')->respond();
    }

    public function destroy($id)
    {
        /** @var NumericalModel $numericalModel */
        $numericalModel = $this->numericalModelRepository->find($id);

        if (empty($numericalModel)) {
            return $this->sendError('NumericalModel not found');
        }

        $this->numericalModelRepository->delete($numericalModel->id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
