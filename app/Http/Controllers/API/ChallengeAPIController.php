<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Interaction\Challenge;
use App\Repositories\ChallengeRepository;


class ChallengeAPIController extends Controller
{

    /**
     * @var ChallengeRepository
     */


    public function __construct(ChallengeRepository $ChallengeRepo)
    {
        $this->ChallengeRepository = $ChallengeRepo;

    }//end __construct()


    private $ChallengeRepository;


    public function getTaskFile($ChallengeID) {
        $Challenge = $this->ChallengeRepository->getById($ChallengeID);
        $taskFileID = $Challenge->files->pluck('id');
        $fileObject = $Challenge->files()->find($taskFileID);
        return \Storage::disk('twill-s3')->temporaryUrl($fileObject->uuid, now()->addMinutes(5));
    }
}
