<?php

namespace App\Http\Requests\Admin;

use A17\Twill\Http\Requests\Admin\Request;

class ChallengeRequest extends Request
{
    public function rulesForCreate()
    {
        return ['challenge_type' => 'required',];
    }

    public function rulesForUpdate()
    {
        $rules = [];
        $rules['subtext'] = 'required';
        $rules['mark'] = 'required';
        
        if ($this['challenge_type'] == "multiple_choice") {
            $rules['option0'] = 'required';
            $rules['option1'] = 'required';
            $rules['option2'] = 'required';
            $rules['option3'] = 'required';
            $rules['correct'] = 'required';
        }
            
        
        return $rules;
    }
}
