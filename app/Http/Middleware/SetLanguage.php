<?php

namespace App\Http\Middleware;

use Closure;

class SetLanguage
{
    public $universalRootPaths = [
        'sanctum',
        'admin',
        'oauth'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() === 'GET') {
            $segment = $request->segment(1);

            if (!in_array($segment, $this->universalRootPaths)) {
                if (!in_array($segment, config('app.available_locales')) && !in_array($segment, $this->universalRootPaths)) {
                    $segments = $request->segments();
                    $fallback = session('locale') ?: config('app.fallback_locale');

                    // PTW: I'm not sure if there is a more standard approach here,
                    // but nothing is jumping out
                    $defaultLocaleForLang = config('app.default_locale_for_lang', []);
                    if (array_key_exists($fallback, $defaultLocaleForLang)) {
                        $fallback = $defaultLocaleForLang[$fallback];
                    }

                    $segments = array_prepend($segments, $fallback);

                    return redirect()->to(implode('/', $segments));
                }

                session(['locale' => $segment]);
                app()->setLocale($segment);
            }
        }
        //\App::setlocale($request->language);
        \Log::info($request->language);
        \Log::info(app()->getLocale());
        \LaravelGettext::setLocale($request->language);
       
        return $next($request);
    }
}
