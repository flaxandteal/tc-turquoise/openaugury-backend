<?php

namespace App\Models\SimulationTier;

use Eloquent as Model;


class FeatureArc extends Model
{


    public $table = 'feature_arcs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'feature_chunk_id',
        'simulation_id',
        'arc'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'feature_chunk_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Simulation that created this arc
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }

    /**
     * CaseFeatureChunk that this arc shows
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function featureChunk()
    {
        return $this->belongsTo(\App\Models\CaseTier\CaseFeatureChunk::class, 'feature_chunk_id', 'id');
    }
}
