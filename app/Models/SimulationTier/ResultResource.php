<?php

namespace App\Models\SimulationTier;

use Eloquent as Model;


/**
 * @SWG\Definition(
 *      definition="ResultResource",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="resource_id",
 *          description="resource_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="duration",
 *          description="duration",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ResultResource extends Model
{


    public $table = 'result_resources';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'resource_id',
        'simulation_id',
        'quantity',
        'duration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'resource_id' => 'integer',
        'duration' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Simulation that created this resource requirement
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }

    /**
     * Resource for which this describes requirements
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function resource()
    {
        return $this->belongsTo(\App\Models\AbstractTier\Resource::class, 'resource_id', 'id');
    }
}
