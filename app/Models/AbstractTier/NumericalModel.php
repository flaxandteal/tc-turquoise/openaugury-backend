<?php

namespace App\Models\AbstractTier;

use Eloquent as Model;

use Alsofronie\Uuid\UuidModelTrait;

/**
 * @SWG\Definition(
 *      definition="NumericalModel",
 *      required={""},
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="definition",
 *          description="definition",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phenomenon_id",
 *          description="phenomenon_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class NumericalModel extends Model
{

    use UuidModelTrait;

    public $table = 'numerical_models';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'definition',
        'phenomenon_id',
        'owner_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'definition' => 'string',
        'phenomenon_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * Owner of this object
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'owner_id', 'id');
    }

    /**
     * Which phenomenon does this model?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function phenomenon()
    {
        return $this->belongsTo(\App\Models\AbstractTier\Phenomenon::class, 'phenomenon_id', 'id');
    }

    /**
     * Get the combinations that use this numerical model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function combinations()
    {
        return $this->hasMany(\App\Models\AbstractTier\Combination::class);
    }

    /**
     * Get analytics that this can provide
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function analytics()
    {
        return $this->belongsToMany(\App\Models\AbstractTier\Analytic::class, 'analytic_numerical_model');
    }

    /**
     * Get a human-readable name including the phenomenon
     *
     * @return string
     */
    public function getDescriptiveNameAttribute()
    {
        if ($this->phenomenon) {
            return $this->phenomenon->name . ' [' . $this->name . ']';
        }
    }
}
