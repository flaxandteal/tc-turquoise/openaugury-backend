<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CaseContextRevision extends Revision
{
    protected $table = "case_context_revisions";
}
