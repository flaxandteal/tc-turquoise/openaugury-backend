<?php

namespace App\Models\Interaction;

use Eloquent as Model;

use Cviebrock\EloquentSluggable\Sluggable;

/**
 * @SWG\Definition(
 *      definition="ChallengeTemplate",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="options",
 *          description="options for user to pick",
 *          type="string",
 *          format="json"
 *      ),
 *      @SWG\Property(
 *          property="correct",
 *          description="correct option",
 *          type="string",
 *          format="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mark",
 *          description="mark",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="analytic_id",
 *          description="analytic_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ChallengeTemplate extends Model
{
    use Sluggable;


    public $table = 'challenge_templates';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'slug',
        'text',
        'subtext',
        'options',
        'correct',
        'mark',
        'global',
        'analytic_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'text' => 'string',
        'subtext' => 'string',
        'analytic_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'text'
            ]
        ];
    }

    /**
     * Analytic, if any, that answers this challenge
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function analytic()
    {
        return $this->belongsTo(\App\Models\AbstractTier\Analytic::class, 'analytic_id', 'id');
    }

    /**
     * Challenges based on this template
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function challenges()
    {
        return $this->hasMany(\App\Models\Interaction\Challenge::class);
    }

    /**
     * Create a challenge from this template
     *
     * @return \App\Models\Interaction\Challenge
     */
    public function fillChallenge(Challenge $challenge)
    {
        $challenge->fill([
            'text' => $this->text,
            'subtext' => $this->subtext,
            'options' => $this->options,
            'correct' => $this->correct,
            'mark' => $this->mark,
            'challenge_type' => $this->challenge_type,
            'challenge_level' => $this->challenge_level,
            'challenge_template_id' => $this->id
        ]);

        return $challenge;
    }
}
