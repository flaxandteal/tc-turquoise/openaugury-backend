<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CaseContextSlug extends Model
{
    protected $table = "case_context_slugs";
}
