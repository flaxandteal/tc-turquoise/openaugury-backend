<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class TargetZoneSlug extends Model
{
    protected $table = "target_zone_slugs";
}
