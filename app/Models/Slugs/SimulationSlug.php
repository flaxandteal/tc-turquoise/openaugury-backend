<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class SimulationSlug extends Model
{
    protected $table = "simulation_slugs";
}
