<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class RegUser extends Model
{
    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['email', 'forename', 'surname'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

}
