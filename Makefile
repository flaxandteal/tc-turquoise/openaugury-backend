CURRENT_DIR = $(shell pwd)
OPENAUGURY_TRANSLATION_I18N = resources/openaugury-translations/resources/lang/i18n
OPENAUGURY_TRANSLATION = $(CURRENT_DIR)/$(OPENAUGURY_TRANSLATION_I18N)

git_push:
	git add .
	git commit -m "$m"
	git push -u origin temp/translation


git_pull:
	git pull

extract_strings:
	bash -c "./dartisan orp:translate";

merge_pot:
	msgcat $(OPENAUGURY_TRANSLATION)/*.pot > $(OPENAUGURY_TRANSLATION)/lang.pot

merge_pot_files:
	msgcat $(OPENAUGURY_TRANSLATION)/*.pot > $(OPENAUGURY_TRANSLATION)/lang.pot

copy_pot_to_language_folders:
	$(shell for d in $(OPENAUGURY_TRANSLATION)/*/ ; do (cp -v $(OPENAUGURY_TRANSLATION)/lang.pot $$d/LC_MESSAGES); done)

change_format_pot_to_po:
	$(shell for d in $(OPENAUGURY_TRANSLATION)/*/ ; do (cd $$d/LC_MESSAGES && mv lang.pot lang_.po); done)

merge_po_files:
	$(shell for d in $(OPENAUGURY_TRANSLATION)/*/ ; do (cd $$d/LC_MESSAGES && msgcat *.po > lang.po); done)

remove_template_po:
	$(shell for d in $(OPENAUGURY_TRANSLATION)/*/ ; do (cd $$d/LC_MESSAGES && rm lang_.po); done)
