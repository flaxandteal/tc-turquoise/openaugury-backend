<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_analytics', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('analytic_id')->unsigned();
            $table->foreign('analytic_id')->references('id')->on('analytics')->onDelete('cascade');

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->json('value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_analytics');
    }
}
