<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwillColumnsToChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->softDeletes();
            $table->boolean('published')->default(false);
            $table->integer('challenge_template_id')->unsigned()->nullable()->change();

            $table->timestamp('publish_start_date')->nullable();
            $table->timestamp('publish_end_date')->nullable();

            $table->boolean('public')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            $table->dropColumn('published');

            $table->dropColumn('publish_start_date');
            $table->dropColumn('publish_end_date');

            $table->dropColumn('public');
        });
    }
}
