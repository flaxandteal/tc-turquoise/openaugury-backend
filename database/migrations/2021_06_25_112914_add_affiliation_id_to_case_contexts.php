<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CaseTier\CaseContext;


class AddAffiliationIdToCaseContexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('case_contexts', function (Blueprint $table) {
            $table->integer('affiliation_id')->nullable();
            $table->foreign('affiliation_id')->references('id')->on('affiliations')->onDelete('cascade');
        });

        CaseContext::with('owner')->get()->map(function ($caseContext) {
            if ($caseContext->owner) {
              $caseContext->affiliation_id = $caseContext->owner->affiliation_id;
              $caseContext->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_contexts', function (Blueprint $table) {
            $table->dropColumn('affiliation_id');
        });
    }
}

