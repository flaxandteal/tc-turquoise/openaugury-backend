<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatisticsColumnToResultFeatureChunk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('result_feature_chunks', function (Blueprint $table) {
            $table->json('statistics')->default('{}');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('result_feature_chunks', function (Blueprint $table) {
            $table->dropColumn('statistics');
        });
    }
}
