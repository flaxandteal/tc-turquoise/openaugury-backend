<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksheetAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worksheet_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->json('first')->nullable();
            $table->json('final')->nullable();
            $table->decimal('mark')->nullable();

            $table->uuid('worksheet_id');
            $table->foreign('worksheet_id')->references('id')->on('worksheets')->onDelete('cascade');

            $table->uuid('challenge_id')->nullable();
            $table->foreign('challenge_id')->references('id')->on('challenges')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worksheet_answers');
    }
}
