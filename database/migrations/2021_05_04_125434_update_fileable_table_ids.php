<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFileableTableIds extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'fileables',
            function (Blueprint $table) {
                $table->dropColumn('fileable_id');
            }
        );

        Schema::table(
            'fileables',
            function (Blueprint $table) {
                $table->uuid('fileable_id')->nullable();
            }
        );

    }//end up()

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'fileables',
            function (Blueprint $table) {
                $table->dropColumn('fileable_id');
            }
        );

        Schema::table(
            'fileables',
            function (Blueprint $table) {
                $table->bigInteger('fileable_id')->nullable()->unsigned();
            }
        );

    }//end down()
}
