<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwillColumnsToFeatureSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feature_sets', function (Blueprint $table) {
            $table->dateTime('deleted_at')->nullable();
            $table->integer('position')->default(0);
            $table->integer('published')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feature_sets', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            $table->dropColumn('position');
            $table->dropColumn('published');
        });
    }
}
