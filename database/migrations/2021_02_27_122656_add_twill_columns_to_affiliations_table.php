<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwillColumnsToAffiliationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('affiliations')->whereNull('district_id')->delete();
        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropForeign('affiliations_district_id_foreign');
        });
        Schema::table('affiliations', function (Blueprint $table) {
            $table->softDeletes();
            $table->boolean('published')->default(false);
            $table->integer('district_id')->unsigned()->change();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('restrict');

            $table->timestamp('publish_start_date')->nullable();
            $table->timestamp('publish_end_date')->nullable();

            $table->boolean('public')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropForeign('affiliations_district_id_foreign');
        });
        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            $table->dropColumn('published');

            $table->dropColumn('publish_start_date');
            $table->dropColumn('publish_end_date');
            $table->integer('district_id')->unsigned()->nullable()->change();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('set null');

            $table->dropColumn('public');
        });
    }
}
