<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worksheets', function (Blueprint $table) {
            $table->uuid('id');
            $table->integer('progress');

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->uuid('worker_id')->nullable();
            $table->foreign('worker_id')->references('id')->on('users')->onDelete('set null');

            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worksheets');
    }
}
