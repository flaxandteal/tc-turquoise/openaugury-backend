<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCachedDataServerFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cached_data_server_features', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cached_data_server_feature_set_id')->unsigned();
            $table->foreign('cached_data_server_feature_set_id')->references('id')->on('cached_data_server_feature_sets')->onDelete('cascade');

            $table->string('feature_id')->nullable();
            $table->point('location')->nullable();
            $table->polygon('extent')->nullable();
            $table->json('json');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cached_data_server_features');
    }
}
