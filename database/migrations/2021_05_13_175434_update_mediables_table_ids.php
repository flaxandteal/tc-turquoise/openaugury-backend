<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMediablesTableIds extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'mediables',
            function (Blueprint $table) {
                $table->dropColumn('mediable_id');
            }
        );

        Schema::table(
            'mediables',
            function (Blueprint $table) {
                $table->uuid('mediable_id')->nullable();
            }
        );

    }//end up()

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'mediables',
            function (Blueprint $table) {
                $table->dropColumn('mediable_id');
            }
        );

        Schema::table(
            'mediables',
            function (Blueprint $table) {
                $table->bigInteger('mediable_id')->nullable()->unsigned();
            }
        );

    }//end down()
}
