<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCombinationAffiliationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combination_affiliation', function (Blueprint $table) {
            $table->increments('id');

            $table->uuid('combination_id');
            $table->foreign('combination_id')->references('id')->on('combinations')->onDelete('cascade');

            $table->integer('affiliation_id')->unsigned();
            $table->foreign('affiliation_id')->references('id')->on('affiliations')->onDelete('cascade');

            $table->integer('status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combination_affiliation');
    }
}
