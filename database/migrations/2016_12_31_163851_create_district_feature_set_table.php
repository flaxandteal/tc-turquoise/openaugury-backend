<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictFeatureSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('district_feature_set', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');

            $table->integer('feature_set_id')->unsigned();
            $table->foreign('feature_set_id')->references('id')->on('feature_sets')->onDelete('cascade');

            $table->integer('status');
            $table->string('data_server_set_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('district_feature_set');
    }
}
