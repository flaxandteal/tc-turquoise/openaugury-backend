<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class SA_DhahranParksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSource = CachedDataServerFeatureSet::whereDataServerSetId('fat-saudi-arabia-dhahran-park')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'Saudi Arabia Dhahran Parks',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/areas-of-archaeological-potential',
              'data_server' => 'orp-ckan',
              'data_server_set_id' => 'fat-saudi-arabia-dhahran-park'
            ]);
        }

        $archPotJson = json_decode(file_get_contents(base_path() . '/resources/opendata/parks_dhahran.geojson'));

        $archPots = GeoJson::jsonUnserialize($archPotJson);

        foreach ($archPots as $archPot) {


            $type = $archPot->getGeometry()->getType(); 
            if ($type == 'Polygon') {
                $coordinates = $archPot->getGeometry()->getCoordinates()[0][0];
            }
            else {
                $coordinates = $archPot->getGeometry()->getCoordinates();
            }
            $feature = new CachedDataServerFeature;
            $properties = $archPot->getProperties();
            $decodedarchPot = json_decode(json_encode($archPot),true);
            $feature->feature_id = $properties['@id'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $postcode = array_key_exists('addr:postcode',$properties) ? $properties['addr:postcode'] : "";
            $housenumber = array_key_exists('addr:housenumber',$properties) ? $properties['addr:housenumber'] : "";
            $street = array_key_exists('addr:street',$properties) ? $properties['addr:street'] : "";
            if (!array_key_exists('name',$properties)) {
                $decodedarchPot["properties"]["name"] = "Park";
            }
            if (array_key_exists('addr:city',$properties)) {
                $decodedarchPot["properties"]['description'] = $properties['addr:city'].";".$postcode." ".$housenumber." ".$street;
            } else {
                $decodedarchPot["properties"]['description'] = $postcode." ".$housenumber." ".$street;
            }
            $feature->json = json_encode($decodedarchPot);
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            //error_log('test4');
            $feature->save();
        }
    }
}
