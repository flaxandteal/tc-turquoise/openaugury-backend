<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class ListedBuildingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $dataSource = CachedDataServerFeatureSet::whereName('Listed Buildings Northern Ireland')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'Listed Buildings Northern Ireland',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/listed-buildings-northern-ireland',
              'data_server' => 'opendatani',
              'data_server_set_id' => 'listed-buildings'
            ]);
        }

        $district = District::whereName('Northern Ireland 1')->first();
        $featureType = FeatureSet::whereSlug('listed-buildings')->first();
        $featureType->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $archPotJson = json_decode(file_get_contents(base_path() . '/resources/opendata/listed-buildings-northern-ireland.geojson'));

        $archPots = GeoJson::jsonUnserialize($archPotJson);
        foreach ($archPots as $archPot) {
            $coordinates = $archPot->getGeometry()->getCoordinates();
            $feature = new CachedDataServerFeature;
            $properties = $archPot->getProperties();
            $feature->feature_id = $properties['OBJECTID'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $feature->json = json_encode($archPot);
            $feature->cached_data_server_feature_set_id = $dataSource->id;

            $feature->save();
        }
    }
}
