<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class SA_DhahranHighwaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSource = CachedDataServerFeatureSet::whereDataServerSetId('fat-saudi-arabia-dhahran-highways')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'Saudi Arabia Dhahran Highways',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/areas-of-archaeological-potential',
              'data_server' => 'orp-ckan',
              'data_server_set_id' => 'fat-saudi-arabia-dhahran-highways'
            ]);
        }

        $archPotJson = json_decode(file_get_contents(base_path() . '/resources/opendata/highways_dhahran.geojson'));

        $archPots = GeoJson::jsonUnserialize($archPotJson);

        foreach ($archPots as $archPot) {

            $type = $archPot->getGeometry()->getType(); 
            if ($type == 'Polygon') {
                $coordinates = $archPot->getGeometry()->getCoordinates()[0][0];
            }
            else {
                $coordinates = $archPot->getGeometry()->getCoordinates();
            }
            $feature = new CachedDataServerFeature;
            $properties = $archPot->getProperties();
            $decodedarchPot = json_decode(json_encode($archPot),true);
            $feature->feature_id = $properties['@id'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $highway = ucwords(str_replace('_',' ',$properties['highway']));
            $decodedarchPot["properties"]["name"] = "Highway";
            $decodedarchPot["properties"]['description'] = $highway;
            $feature->json = json_encode($decodedarchPot);
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            
            $feature->save();
        }
    }
}
