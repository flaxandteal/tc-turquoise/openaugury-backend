<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class SA_RiyadhPublicTransportStopsAndStations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSource = CachedDataServerFeatureSet::whereDataServerSetId('fat-saudi-arabia-public-transport-stop-stations')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'Saudi Arabia Riyadh Public Transport Stops And Stations',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/areas-of-archaeological-potential',
              'data_server' => 'orp-ckan',
              'data_server_set_id' => 'fat-saudi-arabia-public-transport-stop-stations'
            ]);
        }

        $archPotJson = json_decode(file_get_contents(base_path() . '/resources/opendata/public_transport_stops_and_stations_riyadh.geojson'));

        $archPots = GeoJson::jsonUnserialize($archPotJson);

        foreach ($archPots as $archPot) {


            $type = $archPot->getGeometry()->getType(); 
            if ($type == 'Polygon') {
                $coordinates = $archPot->getGeometry()->getCoordinates()[0][0];
            }
            else {
                $coordinates = $archPot->getGeometry()->getCoordinates();
            }
            $feature = new CachedDataServerFeature;
            $properties = $archPot->getProperties();
            $decodedarchPot = json_decode(json_encode($archPot),true);
            $feature->feature_id = $properties['@id'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            if (!array_key_exists('name',$properties)) {
                $decodedarchPot["properties"]['name'] = "Public transport Stops and Stations";
            }
            $decodedarchPot["properties"]['description'] = "Public transport (also known as public transportation, public transit, mass transit, or simply transit) is a system of transport, in contrast to private transport,
             for passengers by group travel systems available for use by the general public, typically managed on a schedule, operated on established routes, and that charge a posted fee for each trip.
             A bus stop is a designated place where buses stop for passengers to get on and off the bus. The construction of bus stops tends to reflect the level of usage,
             where stops at busy locations may have shelters, seating, and possibly electronic passenger information systems; less busy stops may use a simple pole and flag to mark the location.";
            $feature->json = json_encode($decodedarchPot);
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            
            $feature->save();
        }
    }
}
