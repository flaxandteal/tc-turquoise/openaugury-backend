<?php

use Illuminate\Database\Seeder;
use App\Models\AbstractTier\Combination;
use App\Models\AbstractTier\NumericalModel;
use App\Models\AbstractTier\Phenomenon;
use App\Models\Users\Affiliation;
use App\Models\Users\User;

class PhenomenonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $volcano = Phenomenon::firstOrCreate([
          'name' => 'Volcano',
          'symbol' => "/images/icon-volcano.svg",
          'color' => '#e50000'
        ]);
        $sandstorm = Phenomenon::firstOrCreate([
          'name' => 'Sandstorm',
          'symbol' => "/images/icon-sandstorm.svg",
          'color' => '#dcdb64'
        ]);
        $earthquake = Phenomenon::firstOrCreate([
          'name' => 'Earthquake',
          'symbol' => "/images/icon-earthquake.svg",
          'color' => '#c69a19'
        ]);
        Phenomenon::create([
          'name' => 'Flashflood',
          'symbol' => "/images/icon-flood.svg",
          'color' => '#0074e5'
        ]);
        $stormSurge = Phenomenon::firstOrCreate([
          'name' => 'Hurricane Storm Surge',
          'symbol' => "/images/icon-flood.svg",
          'color' => '#0074e5'
        ]);

        $superAdmin = Affiliation::whereName('Super Administrators')->first()->users()->whereForename('Root')->first();

        $stormSurgeModel = NumericalModel::firstOrNew([
            'name' => 'based on Height',
        ]);
        $stormSurgeModel->update([
            'definition' => json_encode([
                'type' => 'surge-elv',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $stormSurge->id
        ]);

        $stormSurgeModel->save();

        $stormSurgeCombination = Combination::firstOrCreate([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $stormSurgeModel->id
        ]);

        $earthquakeModel = NumericalModel::firstOrCreate([
            'name' => 'based on Building Code',
            'definition' => json_encode([
                'type' => 'earthquake-shibata-study',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $earthquake->id
        ]);

        $earthquakeSimpleModel = NumericalModel::firstOrCreate([
            'name' => 'using Danger Rings',
            'definition' => json_encode([
                'type' => 'concentric',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $earthquake->id
        ]);

        $earthquakeCombination = Combination::firstOrCreate([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $earthquakeModel->id
        ]);

        $earthquakeSimpleCombination = Combination::firstOrCreate([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $earthquakeSimpleModel->id
        ]);

        $sandstormModel = NumericalModel::firstOrNew([
            'name' => 'using Simple Drifting Formation',
            'phenomenon_id' => $sandstorm->id
        ]);
        $sandstormModel->fill([
            'definition' => json_encode([
                'type' => 'ss-simple-drift',
                'parameters' => [
                    'speed' => 1e-3,
                    'direction' => 3.14159 / 2
                ]
            ]),
            'owner_id' => $superAdmin->id
        ]);
        $sandstormModel->save();

        $volcanoModel = NumericalModel::firstOrCreate([
            'name' => 'using Ash Danger Rings',
            'definition' => json_encode([
                'type' => 'volcano-ash-deposition',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $volcano->id
        ]);

        $volcanoLandlab = NumericalModel::firstOrCreate([
            'name' => 'based on Height',
            'definition' => json_encode([
                'type' => 'volcano-landlab',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $volcano->id
        ]);

        $volcanoLavaModel = NumericalModel::firstOrCreate([
            'name' => 'using Lava Flow',
            'definition' => json_encode([
                'type' => 'volcano-lava',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $volcano->id
        ]);

        $volcanoCombination = Combination::firstOrCreate([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $volcanoModel->id
        ]);

        $volcanoLavaCombination = Combination::firstOrCreate([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $volcanoLavaModel->id
        ]);

        $volcanoLandlabCombination = Combination::firstOrCreate([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $volcanoLandlab->id
        ]);

        $sandstormSimpleDriftCombination = Combination::firstOrNew([
            'numerical_model_id' => $sandstormModel->id
        ]);
        $sandstormSimpleDriftCombination->fill([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
        ]);
        $sandstormSimpleDriftCombination->save();
    }
}
