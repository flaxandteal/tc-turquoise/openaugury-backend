<?php

use App\Models\Users\User;
use App\Models\Users\Role;
use App\Models\Users\Affiliation;
use App\Models\Features\District;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $affiliation = Affiliation::firstOrCreate(
            [
                'name' => 'Super Administrators'
            ]
        );

        $admin = User::firstOrNew(
            [
                'forename' => 'Root',
                'surname' => 'Administrator',
                'email' => 'admin@example.com',
            ]
        );
				$admin['identifier'] = 'administrator';
				$admin['password'] = 'change-this';
				$admin['affiliation_id'] = $affiliation->id;
				$admin->save();
        $admin->assignRole('admin');
				$admin->save();
        $admin = User::whereEmail('admin@example.com')->first();
				\Log::info($admin->roles);
    }
}
