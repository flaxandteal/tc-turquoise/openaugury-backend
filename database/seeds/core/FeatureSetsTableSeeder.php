<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Illuminate\Database\Seeder;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class FeatureSetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('affiliations')->leftJoin('districts', 'district_id', 'districts.id')->where('districts.name', 'LIKE', '%@%.%')->delete();
        DB::table('districts')->where('name', 'LIKE', '%@%.%')->delete();

        $focalPoint = FeatureSet::firstOrNew([
          'slug' => 'focal-point',
        ]);
        $focalPoint->name = 'Focal Point';
        $focalPoint->description = 'Focus point of the emergency';
        $focalPoint->save();

        //DB::table('district_feature_set')->delete();

        //DB::table('districts')->delete();
        $ni1 = District::firstOrNew([
            'name' => 'Northern Ireland 1',
        ]);

        $ni1->slug = 'ni-1';
        $ni1->save();

        $sa1 = District::firstOrCreate([
            'name' => 'Saudi Arabia 1',
            'slug' => 'sa-1'
        ]);

        $sa2 = District::firstOrCreate([
            'name' => 'Saudi Arabia 2',
            'slug' => 'sa-2'
        ]);
        $sa3 = District::firstOrCreate([
          'name' => 'Saudi Arabia 3',
          'slug' => 'sa-3'
        ]);

        // This is always active for all new simulations (special case)
        // $focalPoint->districts()->sync([
        //     $ni1->id => ['status' => FeatureSet::DISTRICT_ACTIVE],
        //     $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE],
        //     $sa2->id => ['status' => FeatureSet::DISTRICT_ACTIVE]
        // ]);
        //FeatureSet::delete();

        FeatureSet::firstOrCreate([
          'name' => 'Belfast Trees',
          'slug' => 'belfast-trees',
          'description' => 'Preserved trees in belfast'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Regional Waters',
          'slug' => 'regional-waters',
          'description' => 'Regional Waters'
        ]);

        $mySchool = FeatureSet::firstOrNew([
          'slug' => 'my-school',
        ]);
        $mySchool->name = 'My School';
        $mySchool->description = 'Where we learn!';
        $mySchool->save();
        $mySchool->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => '#ref:affiliation-location', 'data_server' => 'local'],
            $sa3->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => '#ref:affiliation-location', 'data_server' => 'local'],
            $ni1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => '#ref:affiliation-location', 'data_server' => 'local']
        ]);
        

        FeatureSet::firstOrCreate([
          'name' => 'School Locations',
          'slug' => 'school-locations',
          'description' => 'Where we learn!'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Bus Stop',
          'slug' => 'bus-stop',
          'description' => 'Pick up and drop off point for buses'
        ]);

        $drainageAsset = FeatureSet::firstOrNew([
          'slug' => 'drainage-assets',
        ]);
        $drainageAsset->name= 'Drainage Assets';
        $drainageAsset->description = 'Drainage for water';
        $drainageAsset->save();

        FeatureSet::firstOrCreate([
          'name' => 'Street Light',
          'slug' => 'street-light',
          'description' => 'Lighting for street users'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Unexplored Cultural Heritage',
          'slug' => 'unexplored-cultural-heritage',
          'description' => 'Mysteries of our past waiting to be discovered'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Industrial Heritage',
          'slug' => 'industrial-heritage',
          'description' => 'Artifact of our industrial past'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Railway Station',
          'slug' => 'railway-station',
          'description' => 'Stopping points for local trains'
        ]);

        /*FeatureSet::firstOrCreate([
          'name' => 'Sports Facility',
          'slug' => 'sports-facility',
          'description' => 'Venue for sports or exercise'
        ]);*/
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'sports-facility',
        ]);
        $featureSet->name = 'Sports Facility';
        $featureSet->description = 'Venue for sports or exercise';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-sports-facility', 'data_server' => 'orp-ckan'],
        ]);
        FeatureSet::firstOrCreate([
          'name' => 'Community Centre',
          'slug' => 'community-centre',
          'description' => 'Venue for community activities'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Waste Site',
          'slug' => 'waste-site',
          'description' => 'Location where waste may be processed or stored'
        ]);

        FeatureSet::firstOrCreate([
          'name' => '90% Risk',
          'slug' => 'result-90-risk',
          'description' => 'Area within which risk of damage is at a nominal 90% level'
        ]);

        FeatureSet::firstOrCreate([
          'name' => '70% Risk',
          'slug' => 'result-70-risk',
          'description' => 'Area within which risk of damage is at a nominal 70% level'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Listed Buildings',
          'slug' => 'listed-buildings',
          'description' => 'A listed building, or listed structure, is one that has been placed on one of the four statutory lists maintained by Historic England in England, Historic Environment Scotland in Scotland, Cadw in Wales, and the Northern Ireland Environment Agency in Northern Ireland.'
        ]);

        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-railway-station',
        ]);
        $featureSet->name = 'Railway Station';
        $featureSet->description = 'Railway Stations features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-railway-station', 'data_server' => 'orp-ckan'],
        ]);

        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-riyadh-park',
        ]);
        $featureSet->name = 'Saudi Arabia Riyadh Park';
        $featureSet->description = 'Saudi Arabia Parks features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-riyadh-park', 'data_server' => 'orp-ckan'],
        ]);

        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-dhahran-park',
        ]);
        $featureSet->name = 'Saudi Arabia Dhahran Park';
        $featureSet->description = 'Saudi Arabia Parks features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa2->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-dhahran-park', 'data_server' => 'orp-ckan'],
        ]);
        
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-riyadh-place-of-worship',
        ]);

        $featureSet->name = 'Riyadh Place Of Worship';
        $featureSet->description = 'Riyadh Place Of Worship features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-riyadh-place-of-worship', 'data_server' => 'orp-ckan'],
        ]);
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-dhahran-place-of-worship',
        ]);
        $featureSet->name = 'Dhahran Place Of Worship';
        $featureSet->description = 'Dhahran Place Of Worship features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa2->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-dhahran-place-of-worship', 'data_server' => 'orp-ckan'],
        ]);

        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-hospitals',
        ]);
        $featureSet->name = 'Hospital';
        $featureSet->description = 'Hospital features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-hospitals', 'data_server' => 'orp-ckan'],
        ]);
        

        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-museums',
        ]);
        $featureSet->name = 'Museum';
        $featureSet->description = 'Museum features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-museums', 'data_server' => 'orp-ckan'],
        ]);
        
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-riyadh-highways',
        ]);
        $featureSet->name = 'Highway';
        $featureSet->description = 'Highway features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-riyadh-highways', 'data_server' => 'orp-ckan'],
        ]);
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-dhahran-highways',
        ]);
        $featureSet->name = 'Highway';
        $featureSet->description = 'Highway features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa2->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-dhahran-highways', 'data_server' => 'orp-ckan'],
        ]);
        
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-schools',
        ]);
        $featureSet->name = 'Schools';
        $featureSet->description = 'School features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-schools', 'data_server' => 'orp-ckan'],
        ]);

        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-public-transport-stop-stations',
        ]);
        $featureSet->name = 'Public Transport Stop Stations';
        $featureSet->description = 'Public transport stop and stations features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-public-transport-stop-stations', 'data_server' => 'orp-ckan'],
        ]);
        $featureSet = FeatureSet::firstOrNew([
          'slug' => 'saudi-arabia-parking',
        ]);
        $featureSet->name = 'Parkings';
        $featureSet->description = 'Saudi Arabia Parking features';
        $featureSet->save();
        $featureSet->districts()->sync([
            $sa1->id => ['status' => FeatureSet::DISTRICT_ACTIVE, 'data_server_set_id' => 'fat-saudi-arabia-parking', 'data_server' => 'orp-ckan'],
        ]);
    }
}
