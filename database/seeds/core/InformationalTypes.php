<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Illuminate\Database\Seeder;
use App\Models\Interaction\InformationalType;

class InformationalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      InformationalType::firstOrCreate([
        'name' => 'Information',
        'slug' => 'information'
      ]);

      InformationalType::firstOrCreate([
        'name' => 'Newsflash',
        'slug' => 'newsflash'
      ]);

      InformationalType::firstOrCreate([
        'name' => 'Social Media',
        'slug' => 'social-media'
      ]);
    }
}
