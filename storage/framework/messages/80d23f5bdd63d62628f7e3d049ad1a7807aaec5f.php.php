<table class="table table-responsive" id="caseContexts-table">
    <thead>
        <th>District Id</th>
        <th>Name</th>
        <th>Center</th>
        <th>Extent</th>
        <th>Begins</th>
        <th>Ends</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $caseContexts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $caseContext): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $caseContext->district_id; ?></td>
            <td><?php echo $caseContext->name; ?></td>
            <td><?php echo $caseContext->center; ?></td>
            <td><?php echo $caseContext->extent; ?></td>
            <td><?php echo $caseContext->begins; ?></td>
            <td><?php echo $caseContext->ends; ?></td>
            <td><?php echo $caseContext->owner_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['caseContexts.destroy', $caseContext->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('caseContexts.show', [$caseContext->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('caseContexts.edit', [$caseContext->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>