<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $featureArc->id; ?></p>
</div>

<!-- Feature Chunk Id Field -->
<div class="form-group">
    <?php echo Form::label('feature_chunk_id', 'Feature Chunk Id:'); ?>

    <p><?php echo $featureArc->feature_chunk_id; ?></p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <p><?php echo $featureArc->simulation_id; ?></p>
</div>

<!-- Arc Field -->
<div class="form-group">
    <?php echo Form::label('arc', 'Arc:'); ?>

    <p><?php echo $featureArc->arc; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $featureArc->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $featureArc->updated_at; ?></p>
</div>

