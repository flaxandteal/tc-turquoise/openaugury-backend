<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $user->id; ?></p>
</div>

<!-- Identifier Field -->
<div class="form-group">
    <?php echo Form::label('identifier', 'Identifier:'); ?>

    <p><?php echo $user->identifier; ?></p>
</div>

<!-- Password Field -->
<div class="form-group">
    <?php echo Form::label('password', 'Password:'); ?>

    <p><?php echo $user->password; ?></p>
</div>

<!-- Affiliation Id Field -->
<div class="form-group">
    <?php echo Form::label('affiliation_id', 'Affiliation Id:'); ?>

    <p><?php echo $user->affiliation_id; ?></p>
</div>

<!-- Teacher Id Field -->
<div class="form-group">
    <?php echo Form::label('teacher_id', 'Teacher Id:'); ?>

    <p><?php echo $user->teacher_id; ?></p>
</div>

<!-- Forename Field -->
<div class="form-group">
    <?php echo Form::label('forename', 'Forename:'); ?>

    <p><?php echo $user->forename; ?></p>
</div>

<!-- Surname Field -->
<div class="form-group">
    <?php echo Form::label('surname', 'Surname:'); ?>

    <p><?php echo $user->surname; ?></p>
</div>

<!-- Email Field -->
<div class="form-group">
    <?php echo Form::label('email', 'Email:'); ?>

    <p><?php echo $user->email; ?></p>
</div>

<!-- Remember Token Field -->
<div class="form-group">
    <?php echo Form::label('remember_token', 'Remember Token:'); ?>

    <p><?php echo $user->remember_token; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $user->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $user->updated_at; ?></p>
</div>

