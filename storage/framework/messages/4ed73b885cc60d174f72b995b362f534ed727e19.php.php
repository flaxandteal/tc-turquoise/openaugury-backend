<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $cachedDataServerFeature->id; ?></p>
</div>

<!-- Cached Data Server Feature Set Id Field -->
<div class="form-group">
    <?php echo Form::label('cached_data_server_feature_set_id', 'Cached Data Server Feature Set Id:'); ?>

    <p><?php echo $cachedDataServerFeature->cached_data_server_feature_set_id; ?></p>
</div>

<!-- Feature Id Field -->
<div class="form-group">
    <?php echo Form::label('feature_id', 'Feature Id:'); ?>

    <p><?php echo $cachedDataServerFeature->feature_id; ?></p>
</div>

<!-- Location Field -->
<div class="form-group">
    <?php echo Form::label('location', 'Location:'); ?>

    <p><?php echo $cachedDataServerFeature->location; ?></p>
</div>

<!-- Extent Field -->
<div class="form-group">
    <?php echo Form::label('extent', 'Extent:'); ?>

    <p><?php echo $cachedDataServerFeature->extent; ?></p>
</div>

<!-- Json Field -->
<div class="form-group">
    <?php echo Form::label('json', 'Json:'); ?>

    <p><?php echo $cachedDataServerFeature->json; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $cachedDataServerFeature->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $cachedDataServerFeature->updated_at; ?></p>
</div>

