<table class="table table-responsive" id="cachedDataServerFeatureSets-table">
    <thead>
        <th>Name</th>
        <th>Owner</th>
        <th>License Title</th>
        <th>License Url</th>
        <th>Uri</th>
        <th>Data Server</th>
        <th>Data Server Set Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $cachedDataServerFeatureSets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cachedDataServerFeatureSet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $cachedDataServerFeatureSet->name; ?></td>
            <td><?php echo $cachedDataServerFeatureSet->owner; ?></td>
            <td><?php echo $cachedDataServerFeatureSet->license_title; ?></td>
            <td><?php echo $cachedDataServerFeatureSet->license_url; ?></td>
            <td><?php echo $cachedDataServerFeatureSet->uri; ?></td>
            <td><?php echo $cachedDataServerFeatureSet->data_server; ?></td>
            <td><?php echo $cachedDataServerFeatureSet->data_server_set_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['cachedDataServerFeatureSets.destroy', $cachedDataServerFeatureSet->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('cachedDataServerFeatureSets.show', [$cachedDataServerFeatureSet->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('cachedDataServerFeatureSets.edit', [$cachedDataServerFeatureSet->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>