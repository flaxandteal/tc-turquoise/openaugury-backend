<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $combination->id; ?></p>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('status', 'Status:'); ?>

    <p><?php echo $combination->status; ?></p>
</div>

<!-- Numerical Model Id Field -->
<div class="form-group">
    <?php echo Form::label('numerical_model_id', 'Numerical Model Id:'); ?>

    <p><?php echo $combination->numerical_model_id; ?></p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <p><?php echo $combination->owner_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $combination->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $combination->updated_at; ?></p>
</div>

