<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CaseContextController extends ModuleController
{
    protected $moduleName = 'caseContexts';

    protected $titleColumnKey = 'name';

    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => true,
        'bulkPublish' => true,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'forceDelete' => true,
        'bulkForceDelete' => true,
        'delete' => true,
        'duplicate' => false,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false, //false to hide this in Add New
        'bulkEdit' => true,
        'editInModal' => false,
    ];

    protected $indexColumns = [
        'name' => [ // field column
            'title' => 'Name',
            'field' => 'name',
        ],
        'email' => [ // field column
            'title' => 'Created',
            'field' => 'created_at',
        ],
        'register' => [ // field column
            'title' => 'Updated',
            'field' => 'updated_at',
        ],
        'switch' => [ // field column
            'title' => 'Edit',
            'field' => 'edit',
        ],
    ];

    /**
     * @param int $id
     * @param \A17\Twill\Models\Model|null $item
     * @return array
     */
    protected function form($id, $item = null)
    {
        $formData = parent::form($id, $item);

        $formData = array_replace_recursive($formData, $this->formData($this->request, $formData['item']));

        return $formData;
    }

    protected function formData($request, $item = null)
    {
        $district = app(\App\Repositories\DistrictRepository::class)->listAll('name');

        if ($item) {
            $featureSetOptions = $item->load('caseFeatureChunks.featureSet')
                ->caseFeatureChunks
                ->pluck('featureSetName', 'id');
        } else {
            $featureSetOptions = [];
        }
        \Log::info($featureSetOptions);

        return [
            'district' => $district,
            'caseFeatureChunksOptions' => $featureSetOptions
        ];
    }

    protected function indexData($request)
    {
        $district = app(\App\Repositories\DistrictRepository::class)->listAll('name');

        return [
            'district' => $district
        ];
    }

}
