<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $resultResource->id; ?></p>
</div>

<!-- Resource Id Field -->
<div class="form-group">
    <?php echo Form::label('resource_id', 'Resource Id:'); ?>

    <p><?php echo $resultResource->resource_id; ?></p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <p><?php echo $resultResource->simulation_id; ?></p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    <?php echo Form::label('quantity', 'Quantity:'); ?>

    <p><?php echo $resultResource->quantity; ?></p>
</div>

<!-- Duration Field -->
<div class="form-group">
    <?php echo Form::label('duration', 'Duration:'); ?>

    <p><?php echo $resultResource->duration; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $resultResource->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $resultResource->updated_at; ?></p>
</div>

