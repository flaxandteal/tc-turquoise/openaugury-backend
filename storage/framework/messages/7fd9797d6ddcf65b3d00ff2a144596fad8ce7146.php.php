<!-- District Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('district_id', 'District Id:'); ?>

    <?php echo Form::number('district_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Center Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('center', 'Center:'); ?>

    <?php echo Form::text('center', null, ['class' => 'form-control']); ?>

</div>

<!-- Extent Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('extent', 'Extent:'); ?>

    <?php echo Form::text('extent', null, ['class' => 'form-control']); ?>

</div>

<!-- Begins Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('begins', 'Begins:'); ?>

    <?php echo Form::date('begins', null, ['class' => 'form-control']); ?>

</div>

<!-- Ends Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('ends', 'Ends:'); ?>

    <?php echo Form::date('ends', null, ['class' => 'form-control']); ?>

</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <?php echo Form::text('owner_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('caseContexts.index'); ?>" class="btn btn-default">Cancel</a>
</div>
