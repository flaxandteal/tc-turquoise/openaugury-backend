<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $challenge->id; ?></p>
</div>

<!-- Text Field -->
<div class="form-group">
    <?php echo Form::label('text', 'Text:'); ?>

    <p><?php echo $challenge->text; ?></p>
</div>

<!-- Subtext Field -->
<div class="form-group">
    <?php echo Form::label('subtext', 'Subtext:'); ?>

    <p><?php echo $challenge->subtext; ?></p>
</div>

<!-- Options Field -->
<div class="form-group">
    <?php echo Form::label('options', 'Options:'); ?>

    <p><?php echo $challenge->options; ?></p>
</div>

<!-- Correct Field -->
<div class="form-group">
    <?php echo Form::label('correct', 'Correct:'); ?>

    <p><?php echo $challenge->correct; ?></p>
</div>

<!-- Mark Field -->
<div class="form-group">
    <?php echo Form::label('mark', 'Mark:'); ?>

    <p><?php echo $challenge->mark; ?></p>
</div>

<!-- Challenge Template Id Field -->
<div class="form-group">
    <?php echo Form::label('challenge_template_id', 'Challenge Template Id:'); ?>

    <p><?php echo $challenge->challenge_template_id; ?></p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <p><?php echo $challenge->case_context_id; ?></p>
</div>

<!-- Result Analytic Id Field -->
<div class="form-group">
    <?php echo Form::label('result_analytic_id', 'Result Analytic Id:'); ?>

    <p><?php echo $challenge->result_analytic_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $challenge->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $challenge->updated_at; ?></p>
</div>

