<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Owner Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('owner', 'Owner:'); ?>

    <?php echo Form::text('owner', null, ['class' => 'form-control']); ?>

</div>

<!-- License Title Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('license_title', 'License Title:'); ?>

    <?php echo Form::text('license_title', null, ['class' => 'form-control']); ?>

</div>

<!-- License Url Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('license_url', 'License Url:'); ?>

    <?php echo Form::text('license_url', null, ['class' => 'form-control']); ?>

</div>

<!-- Uri Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('uri', 'Uri:'); ?>

    <?php echo Form::text('uri', null, ['class' => 'form-control']); ?>

</div>

<!-- Data Server Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('data_server', 'Data Server:'); ?>

    <?php echo Form::text('data_server', null, ['class' => 'form-control']); ?>

</div>

<!-- Data Server Set Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('data_server_set_id', 'Data Server Set Id:'); ?>

    <?php echo Form::text('data_server_set_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('cachedDataServerFeatureSets.index'); ?>" class="btn btn-default">Cancel</a>
</div>
