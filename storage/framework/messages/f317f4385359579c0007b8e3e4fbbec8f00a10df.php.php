<table class="table table-responsive" id="featureArcs-table">
    <thead>
        <th>Feature Chunk Id</th>
        <th>Simulation Id</th>
        <th>Arc</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $featureArcs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $featureArc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $featureArc->feature_chunk_id; ?></td>
            <td><?php echo $featureArc->simulation_id; ?></td>
            <td><?php echo $featureArc->arc; ?></td>
            <td>
                <?php echo Form::open(['route' => ['featureArcs.destroy', $featureArc->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('featureArcs.show', [$featureArc->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('featureArcs.edit', [$featureArc->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>