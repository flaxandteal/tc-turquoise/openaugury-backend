<table class="table table-responsive" id="worksheets-table">
    <thead>
        <th>Progress</th>
        <th>Case Context Id</th>
        <th>Worker Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $worksheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $worksheet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $worksheet->progress; ?></td>
            <td><?php echo $worksheet->case_context_id; ?></td>
            <td><?php echo $worksheet->worker_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['worksheets.destroy', $worksheet->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('worksheets.show', [$worksheet->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('worksheets.edit', [$worksheet->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>