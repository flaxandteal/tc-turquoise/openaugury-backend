<?php $__env->startSection('page-wrapper'); ?>
        
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo e(\Lang::get('panel::fields.dashboard')); ?></h1>
                    <div class="icon-bg ic-layers"></div>
                </div>
                            
            </div>
            <!-- /.row -->
            <div class="row box-holder">
                
                <?php if(is_array(\Serverfireteam\Panel\Link::pluck('url'))): ?>
                    <?php if(Auth::user()->affiliation): ?>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-7 title">
                                            Affiliation
                                        </div>
                                        <div class="col-xs-5 text-right">
                                        <div class="huge"><?php echo e(Auth::user()->affiliation->name); ?></div>
                                        <div class="large">Affiliation Code for User Registration: <span style='white-space: nowrap'><?php echo e(Auth::user()->affiliation->code); ?></span></div>

                                        </div>
                                    </div>
                                </div>
                                    <div class="panel-footer">

                                         <a href='<?php echo e(url('panel/edit')); ?>' class="pull-left"><?php echo e(\Lang::get('panel::fields.ProfileEdit')); ?> <i class="icon ic-chevron-right"></i></a>
                                        <div class="pull-right"> </div>

                                        <div class="clearfix"></div>
                                    </div>
                            </div>
                        </div>
                        <?php if(Auth::user()->affiliation->isMetered()): ?>
                            <div class="col-lg-6 col-md-6">
                                <div class="panel ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-7 title">
                                                Affiliation Remaining Balance
                                            </div>
                                            <div class="col-xs-5 text-right">
                                            <div class="huge"><?php echo e(Auth::user()->affiliation->simulations_remaining); ?> Credits</div>
                                                <div class="large">Simulations and certain other services are metered. Contact us for more information on service offerings.</div>

                                            </div>
                                        </div>
                                    </div>
                                        <div class="panel-footer">

                                             <a href='<?php echo e(url('panel/edit')); ?>' class="pull-left"><?php echo e(\Lang::get('panel::fields.ProfileEdit')); ?> <i class="icon ic-chevron-right"></i></a>
                                            <div class="pull-right"> <a class="add " href="<?php echo e(url('panel/edit')); ?>"><?php echo e(\Lang::get('panel::fields.Add')); ?> CREDIT </a></div>

                                            <div class="clearfix"></div>
                                        </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php $__currentLoopData = Serverfireteam\Panel\libs\dashboard::getItems(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $box): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel ">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-7 title">
                                        <?php echo e($box['title']); ?>

                                    </div>
                                    <div class="col-xs-5 text-right">
                                        <div class="huge"><?php echo e($box['count']); ?></div>
                                        <div></div>

                                    </div>
                                </div>
                            </div>
                                <div class="panel-footer">

                                     <a href='<?php echo e($box['showListUrl']); ?>' class="pull-left"><?php echo e(\Lang::get('panel::fields.showList')); ?> <i class="icon ic-chevron-right"></i></a>
                                    <div class="pull-right"> <a class="add " href="<?php echo e($box['addUrl']); ?>"><?php echo e(\Lang::get('panel::fields.Add')); ?>  </a></div>

                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>


            </div>

<script>
    $(function(){
        var color = ['primary','green','orange','red','purple','green2','blue2','yellow'];
        var pointer = 0;
        $('.panel').each(function(){
            if(pointer > color.length) pointer = 0;
            $(this).addClass('panel-'+color[pointer]);
            $(this).find('.pull-right .add').addClass('panel-'+color[pointer]);
            pointer++;
        })
        
    })
</script>
<?php $__env->stopSection(); ?>            

<?php echo $__env->make('panelViews::mainTemplate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>