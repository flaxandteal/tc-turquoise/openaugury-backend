<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Definition Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('definition', 'Definition:'); ?>

    <?php echo Form::textarea('definition', null, ['class' => 'form-control']); ?>

</div>

<!-- Phenomenon Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('phenomenon_id', 'Phenomenon Id:'); ?>

    <?php echo Form::number('phenomenon_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <?php echo Form::text('owner_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('numericalModels.index'); ?>" class="btn btn-default">Cancel</a>
</div>
