<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <?php echo Form::text('case_context_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Combination Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('combination_id', 'Combination Id:'); ?>

    <?php echo Form::text('combination_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Settings Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('settings', 'Settings:'); ?>

    <?php echo Form::textarea('settings', null, ['class' => 'form-control']); ?>

</div>

<!-- Definition Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('definition', 'Definition:'); ?>

    <?php echo Form::textarea('definition', null, ['class' => 'form-control']); ?>

</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <?php echo Form::number('status', null, ['class' => 'form-control']); ?>

</div>

<!-- Center Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('center', 'Center:'); ?>

    <?php echo Form::text('center', null, ['class' => 'form-control']); ?>

</div>

<!-- Extent Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('extent', 'Extent:'); ?>

    <?php echo Form::text('extent', null, ['class' => 'form-control']); ?>

</div>

<!-- Begins Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('begins', 'Begins:'); ?>

    <?php echo Form::date('begins', null, ['class' => 'form-control']); ?>

</div>

<!-- Ends Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('ends', 'Ends:'); ?>

    <?php echo Form::date('ends', null, ['class' => 'form-control']); ?>

</div>

<!-- Result Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('result', 'Result:'); ?>

    <?php echo Form::textarea('result', null, ['class' => 'form-control']); ?>

</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <?php echo Form::text('owner_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('simulations.index'); ?>" class="btn btn-default">Cancel</a>
</div>
