<table class="table table-responsive" id="districts-table">
    <thead>
        <th><?php echo e(_i('Slug')); ?></th>
        <th><?php echo e(_i('Name')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $districts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $district->slug; ?></td>
            <td><?php echo $district->name; ?></td>
            <td>
                <?php echo Form::open(['route' => ['districts.destroy', $district->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('districts.show', [$district->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('districts.edit', [$district->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>