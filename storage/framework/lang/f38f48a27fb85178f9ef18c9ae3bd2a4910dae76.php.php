<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $caseFeatureChunk->id; ?></p>
</div>

<!-- Chunk Field -->
<div class="form-group">
    <?php echo Form::label('chunk', 'Chunk:'); ?>

    <p><?php echo $caseFeatureChunk->chunk; ?></p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <p><?php echo $caseFeatureChunk->case_context_id; ?></p>
</div>

<!-- Feature Set Id Field -->
<div class="form-group">
    <?php echo Form::label('feature_set_id', 'Feature Set Id:'); ?>

    <p><?php echo $caseFeatureChunk->feature_set_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $caseFeatureChunk->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $caseFeatureChunk->updated_at; ?></p>
</div>

