<!-- Text Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('text', 'Text:'); ?>

    <?php echo Form::text('text', null, ['class' => 'form-control']); ?>

</div>

<!-- Subtext Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('subtext', 'Subtext:'); ?>

    <?php echo Form::textarea('subtext', null, ['class' => 'form-control']); ?>

</div>

<!-- Options Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('options', 'Options:'); ?>

    <?php echo Form::textarea('options', null, ['class' => 'form-control']); ?>

</div>

<!-- Correct Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('correct', 'Correct:'); ?>

    <?php echo Form::textarea('correct', null, ['class' => 'form-control']); ?>

</div>

<!-- Mark Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('mark', 'Mark:'); ?>

    <?php echo Form::number('mark', null, ['class' => 'form-control']); ?>

</div>

<!-- Challenge Template Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('challenge_template_id', 'Challenge Template Id:'); ?>

    <?php echo Form::number('challenge_template_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <?php echo Form::text('case_context_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Result Analytic Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('result_analytic_id', 'Result Analytic Id:'); ?>

    <?php echo Form::number('result_analytic_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('challenges.index'); ?>" class="btn btn-default"><?php echo e(_i('Cancel')); ?></a>
</div>
