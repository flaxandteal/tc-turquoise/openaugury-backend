<!-- Progress Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('progress', 'Progress:'); ?>

    <?php echo Form::number('progress', null, ['class' => 'form-control']); ?>

</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <?php echo Form::text('case_context_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Worker Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('worker_id', 'Worker Id:'); ?>

    <?php echo Form::text('worker_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('worksheets.index'); ?>" class="btn btn-default">Cancel</a>
</div>
