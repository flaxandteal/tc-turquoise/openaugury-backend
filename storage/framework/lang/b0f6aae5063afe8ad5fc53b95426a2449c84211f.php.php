<!-- Chunk Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('chunk', 'Chunk:'); ?>

    <?php echo Form::textarea('chunk', null, ['class' => 'form-control']); ?>

</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <?php echo Form::text('simulation_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Feature Set Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('feature_set_id', 'Feature Set Id:'); ?>

    <?php echo Form::number('feature_set_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('resultFeatureChunks.index'); ?>" class="btn btn-default">Cancel</a>
</div>
