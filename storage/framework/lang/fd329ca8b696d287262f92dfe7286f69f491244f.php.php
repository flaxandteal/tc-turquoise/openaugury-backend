<table class="table table-responsive" id="affiliations-table">
    <thead>
        <th><?php echo e(_i('Slug')); ?></th>
        <th><?php echo e(_i('Name')); ?></th>
        <th><?php echo e(_i('District Id')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $affiliations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $affiliation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $affiliation->slug; ?></td>
            <td><?php echo $affiliation->name; ?></td>
            <td><?php echo $affiliation->district_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['affiliations.destroy', $affiliation->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('affiliations.show', [$affiliation->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('affiliations.edit', [$affiliation->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>