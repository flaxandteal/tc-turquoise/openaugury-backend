<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $affiliation->id; ?></p>
</div>

<!-- Slug Field -->
<div class="form-group">
    <?php echo Form::label('slug', 'Slug:'); ?>

    <p><?php echo $affiliation->slug; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $affiliation->name; ?></p>
</div>

<!-- District Id Field -->
<div class="form-group">
    <?php echo Form::label('district_id', 'District Id:'); ?>

    <p><?php echo $affiliation->district_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $affiliation->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $affiliation->updated_at; ?></p>
</div>

