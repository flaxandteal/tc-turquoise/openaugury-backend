<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $worksheet->id; ?></p>
</div>

<!-- Progress Field -->
<div class="form-group">
    <?php echo Form::label('progress', 'Progress:'); ?>

    <p><?php echo $worksheet->progress; ?></p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <p><?php echo $worksheet->case_context_id; ?></p>
</div>

<!-- Worker Id Field -->
<div class="form-group">
    <?php echo Form::label('worker_id', 'Worker Id:'); ?>

    <p><?php echo $worksheet->worker_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $worksheet->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $worksheet->updated_at; ?></p>
</div>

