<?php $__env->startSection('bodyClass'); ?>
register
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    
                    <div class="login-panel panel panel-default animated fadeInDown">
                     
                        <div class="panel-body">
                            <div class="logo-holder">
                                <img src="<?php echo e(asset(Config::get('panel.logo'))); ?>" />
                            </div>
                            <?php echo Form::open(array('url' => 'register')); ?>

                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.email')); ?>" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.password')); ?>" name="password" type="password" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit"  class="btn btn-lg btn-success btn-block" value="Register">
                                </fieldset>
                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('panelViews::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>