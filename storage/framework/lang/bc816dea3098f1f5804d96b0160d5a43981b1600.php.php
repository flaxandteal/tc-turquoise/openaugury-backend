<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $numericalModel->id; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $numericalModel->name; ?></p>
</div>

<!-- Definition Field -->
<div class="form-group">
    <?php echo Form::label('definition', 'Definition:'); ?>

    <p><?php echo $numericalModel->definition; ?></p>
</div>

<!-- Phenomenon Id Field -->
<div class="form-group">
    <?php echo Form::label('phenomenon_id', 'Phenomenon Id:'); ?>

    <p><?php echo $numericalModel->phenomenon_id; ?></p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <p><?php echo $numericalModel->owner_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $numericalModel->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $numericalModel->updated_at; ?></p>
</div>

