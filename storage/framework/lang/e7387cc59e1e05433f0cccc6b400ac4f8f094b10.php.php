<?php $__env->startSection('title'); ?>
<div class="title row">
    Our Chaotic World in Context
</div>
<div class="subtitle row">
    Bringing social, economic and cultural resilience to life
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="bg">
    <div class="splash bg big container-fluid" style="background-image: url('<?php echo e(config("openaugury.brand.splash.url-default")); ?>')">
        <div class="info container-fluid">
            <div class="row">
                <div class="col m-4">
                    <?php echo $__env->yieldContent('title'); ?>
                </div>
                <div class="action col text-right m-4 mt-auto col-3">
                    <a href="v/" class="btn btn-outline-light btn-lg" type="button">View simulations</a>
                </div>
            </div>
        </div>
        <div class="image-attribution">
            <a href="<?php echo e(config('openaugury.brand.splash.attribution-url')); ?>"><?php echo e(config('openaugury.brand.splash.attribution-text')); ?></a>
        </div>
    </div>
    <div class="splash bg small container-fluid" style="background-image: url('<?php echo e(config("openaugury.brand.splash.url-small")); ?>')">
        <div class="info">
            <?php echo $__env->yieldContent('title'); ?>
            <div class="action row">
                <a href="v/" class="btn btn-outline-light btn-lg" type="button">View simulations</a>
            </div>
        </div>
        <div class="image-attribution">
            <a href="<?php echo e(config('openaugury.brand.splash.attribution-url')); ?>"><?php echo e(config('openaugury.brand.splash.attribution-text')); ?></a>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>