<?php $__env->startSection('bodyClass'); ?>
register
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    
                    <div class="login-panel panel panel-default">
                     
                        <div class="panel-body">
                            <div class="logo-holder">
                                <img src="<?php echo e(asset(Config::get('panel.logo'))); ?>" />
                            </div>
                            <?php echo Form::open(array('route' => 'users.store')); ?>

                                <fieldset>
                                    <div class="form-group">
                                        <label for="district_id">District: </label>
                                        <select class="form-control" placeholder="District ID" name="district_id" type="text" autofocus>
                                            <?php $__currentLoopData = $districts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($district->id); ?>"><?php echo e($district->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="affiliation_name">Affiliation Name: </label>
                                        <input class="form-control" placeholder="Affiliation Name" name="affiliation_name" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">First Name:</label>
                                        <input class="form-control" placeholder="First Name" name="first_name" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">Last Name:</label>
                                        <input class="form-control" placeholder="Last Name" name="last_name" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">Email:</label>
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.email')); ?>" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">Password:</label>
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.password')); ?>" name="password" type="password" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit"  class="btn btn-lg btn-success btn-block" value="Register">
                                </fieldset>
                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('panelViews::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>