<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $challengeTemplate->id; ?></p>
</div>

<!-- Slug Field -->
<div class="form-group">
    <?php echo Form::label('slug', 'Slug:'); ?>

    <p><?php echo $challengeTemplate->slug; ?></p>
</div>

<!-- Text Field -->
<div class="form-group">
    <?php echo Form::label('text', 'Text:'); ?>

    <p><?php echo $challengeTemplate->text; ?></p>
</div>

<!-- Subtext Field -->
<div class="form-group">
    <?php echo Form::label('subtext', 'Subtext:'); ?>

    <p><?php echo $challengeTemplate->subtext; ?></p>
</div>

<!-- Options Field -->
<div class="form-group">
    <?php echo Form::label('options', 'Options:'); ?>

    <p><?php echo $challengeTemplate->options; ?></p>
</div>

<!-- Correct Field -->
<div class="form-group">
    <?php echo Form::label('correct', 'Correct:'); ?>

    <p><?php echo $challengeTemplate->correct; ?></p>
</div>

<!-- Mark Field -->
<div class="form-group">
    <?php echo Form::label('mark', 'Mark:'); ?>

    <p><?php echo $challengeTemplate->mark; ?></p>
</div>

<!-- Global Field -->
<div class="form-group">
    <?php echo Form::label('global', 'Global:'); ?>

    <p><?php echo $challengeTemplate->global; ?></p>
</div>

<!-- Analytic Id Field -->
<div class="form-group">
    <?php echo Form::label('analytic_id', 'Analytic Id:'); ?>

    <p><?php echo $challengeTemplate->analytic_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $challengeTemplate->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $challengeTemplate->updated_at; ?></p>
</div>

