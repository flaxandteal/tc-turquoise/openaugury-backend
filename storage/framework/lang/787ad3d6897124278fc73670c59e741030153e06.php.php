<table class="table table-responsive" id="combinations-table">
    <thead>
        <th><?php echo e(_i('Status')); ?></th>
        <th><?php echo e(_i('Numerical Model Id')); ?></th>
        <th><?php echo e(_i('Owner Id')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $combinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $combination): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $combination->status; ?></td>
            <td><?php echo $combination->numerical_model_id; ?></td>
            <td><?php echo $combination->owner_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['combinations.destroy', $combination->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('combinations.show', [$combination->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('combinations.edit', [$combination->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>