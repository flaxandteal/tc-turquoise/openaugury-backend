<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>
        <?php echo e(_i('Cached Data Server Feature')); ?>

        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <?php echo $__env->make('cached_data_server_features.show_fields', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <a href="<?php echo route('cachedDataServerFeatures.index'); ?>" class="btn btn-default"><?php echo e(_i('Back')); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>