<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $resultFeatureChunk->id; ?></p>
</div>

<!-- Chunk Field -->
<div class="form-group">
    <?php echo Form::label('chunk', 'Chunk:'); ?>

    <p><?php echo $resultFeatureChunk->chunk; ?></p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <p><?php echo $resultFeatureChunk->simulation_id; ?></p>
</div>

<!-- Feature Set Id Field -->
<div class="form-group">
    <?php echo Form::label('feature_set_id', 'Feature Set Id:'); ?>

    <p><?php echo $resultFeatureChunk->feature_set_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $resultFeatureChunk->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $resultFeatureChunk->updated_at; ?></p>
</div>

