<table class="table table-responsive" id="resultFeatureChunks-table">
    <thead>
        <th>Chunk</th>
        <th>Simulation Id</th>
        <th>Feature Set Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $resultFeatureChunks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resultFeatureChunk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $resultFeatureChunk->chunk; ?></td>
            <td><?php echo $resultFeatureChunk->simulation_id; ?></td>
            <td><?php echo $resultFeatureChunk->feature_set_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['resultFeatureChunks.destroy', $resultFeatureChunk->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('resultFeatureChunks.show', [$resultFeatureChunk->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('resultFeatureChunks.edit', [$resultFeatureChunk->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>