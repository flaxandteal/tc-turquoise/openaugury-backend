<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class TargetZoneController extends ModuleController
{
    protected $moduleName = 'target_Zones';
}
