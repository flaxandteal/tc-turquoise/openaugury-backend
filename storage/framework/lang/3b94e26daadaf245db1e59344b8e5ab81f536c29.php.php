<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $worksheetAnswer->id; ?></p>
</div>

<!-- First Field -->
<div class="form-group">
    <?php echo Form::label('first', 'First:'); ?>

    <p><?php echo $worksheetAnswer->first; ?></p>
</div>

<!-- Final Field -->
<div class="form-group">
    <?php echo Form::label('final', 'Final:'); ?>

    <p><?php echo $worksheetAnswer->final; ?></p>
</div>

<!-- Mark Field -->
<div class="form-group">
    <?php echo Form::label('mark', 'Mark:'); ?>

    <p><?php echo $worksheetAnswer->mark; ?></p>
</div>

<!-- Worksheet Id Field -->
<div class="form-group">
    <?php echo Form::label('worksheet_id', 'Worksheet Id:'); ?>

    <p><?php echo $worksheetAnswer->worksheet_id; ?></p>
</div>

<!-- Challenge Id Field -->
<div class="form-group">
    <?php echo Form::label('challenge_id', 'Challenge Id:'); ?>

    <p><?php echo $worksheetAnswer->challenge_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $worksheetAnswer->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $worksheetAnswer->updated_at; ?></p>
</div>

