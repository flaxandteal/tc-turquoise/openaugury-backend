<table class="table table-responsive" id="featureSets-table">
    <thead>
        <th><?php echo e(_i('Slug')); ?></th>
        <th><?php echo e(_i('Name')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $featureSets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $featureSet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $featureSet->slug; ?></td>
            <td><?php echo $featureSet->name; ?></td>
            <td>
                <?php echo Form::open(['route' => ['featureSets.destroy', $featureSet->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('featureSets.show', [$featureSet->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('featureSets.edit', [$featureSet->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>