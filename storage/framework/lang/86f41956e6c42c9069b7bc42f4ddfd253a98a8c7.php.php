<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $resultAnalytic->id; ?></p>
</div>

<!-- Analytic Id Field -->
<div class="form-group">
    <?php echo Form::label('analytic_id', 'Analytic Id:'); ?>

    <p><?php echo $resultAnalytic->analytic_id; ?></p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <p><?php echo $resultAnalytic->simulation_id; ?></p>
</div>

<!-- Value Field -->
<div class="form-group">
    <?php echo Form::label('value', 'Value:'); ?>

    <p><?php echo $resultAnalytic->value; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $resultAnalytic->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $resultAnalytic->updated_at; ?></p>
</div>

