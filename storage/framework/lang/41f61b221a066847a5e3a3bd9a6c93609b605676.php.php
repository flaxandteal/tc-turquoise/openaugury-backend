<!-- Feature Chunk Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('feature_chunk_id', 'Feature Chunk Id:'); ?>

    <?php echo Form::number('feature_chunk_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <?php echo Form::text('simulation_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Arc Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('arc', 'Arc:'); ?>

    <?php echo Form::textarea('arc', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('featureArcs.index'); ?>" class="btn btn-default"><?php echo e(_i('Cancel')); ?></a>
</div>
