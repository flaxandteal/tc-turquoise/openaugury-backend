<!-- Slug Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('slug', 'Slug:'); ?>

    <?php echo Form::text('slug', null, ['class' => 'form-control']); ?>

</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Symbol Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('symbol', 'Symbol:'); ?>

    <?php echo Form::text('symbol', null, ['class' => 'form-control']); ?>

</div>

<!-- Color Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('color', 'Color:'); ?>

    <?php echo Form::text('color', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('phenomenons.index'); ?>" class="btn btn-default">Cancel</a>
</div>
