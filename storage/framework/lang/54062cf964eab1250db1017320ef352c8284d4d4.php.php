<!-- Chunk Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('chunk', 'Chunk:'); ?>

    <?php echo Form::textarea('chunk', null, ['class' => 'form-control']); ?>

</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <?php echo Form::text('case_context_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Feature Set Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('feature_set_id', 'Feature Set Id:'); ?>

    <?php echo Form::number('feature_set_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('caseFeatureChunks.index'); ?>" class="btn btn-default"><?php echo e(_i('Cancel')); ?></a>
</div>
