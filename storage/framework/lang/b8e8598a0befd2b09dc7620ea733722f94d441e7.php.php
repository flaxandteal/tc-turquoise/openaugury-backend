<table class="table table-responsive" id="cachedDataServerFeatures-table">
    <thead>
        <th><?php echo e(_i('Cached Data Server Feature Set Id')); ?></th>
        <th><?php echo e(_i('Feature Id')); ?></th>
        <th><?php echo e(_i('Location')); ?></th>
        <th><?php echo e(_i('Extent')); ?></th>
        <th><?php echo e(_i('Json')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $cachedDataServerFeatures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cachedDataServerFeature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $cachedDataServerFeature->cached_data_server_feature_set_id; ?></td>
            <td><?php echo $cachedDataServerFeature->feature_id; ?></td>
            <td><?php echo $cachedDataServerFeature->location; ?></td>
            <td><?php echo $cachedDataServerFeature->extent; ?></td>
            <td><?php echo $cachedDataServerFeature->json; ?></td>
            <td>
                <?php echo Form::open(['route' => ['cachedDataServerFeatures.destroy', $cachedDataServerFeature->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('cachedDataServerFeatures.show', [$cachedDataServerFeature->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('cachedDataServerFeatures.edit', [$cachedDataServerFeature->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>