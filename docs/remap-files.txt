--- App\Models\AbstractTier\Analytic App\Models\Analytic ---
app/Http/Controllers/API/AnalyticAPIController.php
app/Http/Requests/API/CreateAnalyticAPIRequest.php
app/Http/Requests/API/UpdateAnalyticAPIRequest.php
app/Http/Requests/CreateAnalyticRequest.php
app/Http/Requests/UpdateAnalyticRequest.php
app/Repositories/AnalyticRepository.php
tests/AnalyticRepositoryTest.php
tests/traits/MakeAnalyticTrait.php
--- App\Models\CaseTier\CaseContext App\Models\CaseContext ---
app/Http/Controllers/API/CaseContextAPIController.php
app/Http/Requests/API/CreateCaseContextAPIRequest.php
app/Http/Requests/API/UpdateCaseContextAPIRequest.php
app/Http/Requests/CreateCaseContextRequest.php
app/Http/Requests/UpdateCaseContextRequest.php
app/Repositories/CaseContextRepository.php
tests/CaseContextRepositoryTest.php
tests/traits/MakeCaseContextTrait.php
--- App\Models\CaseTier\CaseFeatureChunk App\Models\CaseFeatureChunk ---
app/Http/Controllers/API/CaseFeatureChunkAPIController.php
app/Http/Requests/API/CreateCaseFeatureChunkAPIRequest.php
app/Http/Requests/API/UpdateCaseFeatureChunkAPIRequest.php
app/Http/Requests/CreateCaseFeatureChunkRequest.php
app/Http/Requests/UpdateCaseFeatureChunkRequest.php
app/Repositories/CaseFeatureChunkRepository.php
tests/CaseFeatureChunkRepositoryTest.php
tests/traits/MakeCaseFeatureChunkTrait.php
--- App\Models\Interaction\Challenge App\Models\Challenge ---
app/Http/Controllers/API/ChallengeAPIController.php
app/Http/Controllers/API/ChallengeTemplateAPIController.php
app/Http/Requests/API/CreateChallengeAPIRequest.php
app/Http/Requests/API/CreateChallengeTemplateAPIRequest.php
app/Http/Requests/API/UpdateChallengeAPIRequest.php
app/Http/Requests/API/UpdateChallengeTemplateAPIRequest.php
app/Http/Requests/CreateChallengeRequest.php
app/Http/Requests/CreateChallengeTemplateRequest.php
app/Http/Requests/UpdateChallengeRequest.php
app/Http/Requests/UpdateChallengeTemplateRequest.php
app/Repositories/ChallengeRepository.php
app/Repositories/ChallengeTemplateRepository.php
tests/ChallengeRepositoryTest.php
tests/ChallengeTemplateRepositoryTest.php
tests/traits/MakeChallengeTemplateTrait.php
tests/traits/MakeChallengeTrait.php
--- App\Models\Interaction\ChallengeTemplate App\Models\ChallengeTemplate ---
app/Http/Controllers/API/ChallengeTemplateAPIController.php
app/Http/Requests/API/CreateChallengeTemplateAPIRequest.php
app/Http/Requests/API/UpdateChallengeTemplateAPIRequest.php
app/Http/Requests/CreateChallengeTemplateRequest.php
app/Http/Requests/UpdateChallengeTemplateRequest.php
app/Repositories/ChallengeTemplateRepository.php
tests/ChallengeTemplateRepositoryTest.php
tests/traits/MakeChallengeTemplateTrait.php
--- App\Models\AbstractTier\Combination App\Models\Combination ---
app/Http/Controllers/API/CombinationAPIController.php
app/Http/Requests/API/CreateCombinationAPIRequest.php
app/Http/Requests/API/UpdateCombinationAPIRequest.php
app/Http/Requests/CreateCombinationRequest.php
app/Http/Requests/UpdateCombinationRequest.php
app/Repositories/CombinationRepository.php
tests/CombinationRepositoryTest.php
tests/traits/MakeCombinationTrait.php
--- App\Models\AbstractTier\District App\Models\District ---
app/Http/Controllers/API/DistrictAPIController.php
app/Http/Requests/API/CreateDistrictAPIRequest.php
app/Http/Requests/API/UpdateDistrictAPIRequest.php
app/Http/Requests/CreateDistrictRequest.php
app/Http/Requests/UpdateDistrictRequest.php
app/Repositories/DistrictRepository.php
tests/DistrictRepositoryTest.php
tests/traits/MakeDistrictTrait.php
--- App\Models\SimulationTier\FeatureArc App\Models\FeatureArc ---
app/Http/Controllers/API/FeatureArcAPIController.php
app/Http/Requests/API/CreateFeatureArcAPIRequest.php
app/Http/Requests/API/UpdateFeatureArcAPIRequest.php
app/Http/Requests/CreateFeatureArcRequest.php
app/Http/Requests/UpdateFeatureArcRequest.php
app/Repositories/FeatureArcRepository.php
tests/FeatureArcRepositoryTest.php
tests/traits/MakeFeatureArcTrait.php
--- App\Models\Features\FeatureSet App\Models\FeatureSet ---
app/Http/Controllers/API/FeatureSetAPIController.php
app/Http/Requests/API/CreateFeatureSetAPIRequest.php
app/Http/Requests/API/UpdateFeatureSetAPIRequest.php
app/Http/Requests/CreateFeatureSetRequest.php
app/Http/Requests/UpdateFeatureSetRequest.php
app/Repositories/FeatureSetRepository.php
tests/FeatureSetRepositoryTest.php
tests/traits/MakeFeatureSetTrait.php
--- App\Models\Interaction\Informational App\Models\Informational ---
app/Http/Controllers/API/InformationalAPIController.php
app/Http/Controllers/API/InformationalTypeAPIController.php
app/Http/Requests/API/CreateInformationalAPIRequest.php
app/Http/Requests/API/CreateInformationalTypeAPIRequest.php
app/Http/Requests/API/UpdateInformationalAPIRequest.php
app/Http/Requests/API/UpdateInformationalTypeAPIRequest.php
app/Http/Requests/CreateInformationalRequest.php
app/Http/Requests/CreateInformationalTypeRequest.php
app/Http/Requests/UpdateInformationalRequest.php
app/Http/Requests/UpdateInformationalTypeRequest.php
app/Repositories/InformationalRepository.php
app/Repositories/InformationalTypeRepository.php
tests/InformationalRepositoryTest.php
tests/InformationalTypeRepositoryTest.php
tests/traits/MakeInformationalTrait.php
tests/traits/MakeInformationalTypeTrait.php
--- App\Models\Interaction\InformationalType App\Models\InformationalType ---
app/Http/Controllers/API/InformationalTypeAPIController.php
app/Http/Requests/API/CreateInformationalTypeAPIRequest.php
app/Http/Requests/API/UpdateInformationalTypeAPIRequest.php
app/Http/Requests/CreateInformationalTypeRequest.php
app/Http/Requests/UpdateInformationalTypeRequest.php
app/Repositories/InformationalTypeRepository.php
tests/InformationalTypeRepositoryTest.php
tests/traits/MakeInformationalTypeTrait.php
--- App\Models\Users\Affiliation App\Models\Affiliation ---
app/Http/Controllers/API/AffiliationAPIController.php
app/Http/Requests/API/CreateAffiliationAPIRequest.php
app/Http/Requests/API/UpdateAffiliationAPIRequest.php
app/Http/Requests/CreateAffiliationRequest.php
app/Http/Requests/UpdateAffiliationRequest.php
app/Repositories/AffiliationRepository.php
tests/AffiliationRepositoryTest.php
tests/traits/MakeAffiliationTrait.php
--- App\Models\AbstractTier\NumericalModel App\Models\NumericalModel ---
app/Http/Controllers/API/NumericalModelAPIController.php
app/Http/Requests/API/CreateNumericalModelAPIRequest.php
app/Http/Requests/API/UpdateNumericalModelAPIRequest.php
app/Http/Requests/CreateNumericalModelRequest.php
app/Http/Requests/UpdateNumericalModelRequest.php
app/Repositories/NumericalModelRepository.php
tests/NumericalModelRepositoryTest.php
tests/traits/MakeNumericalModelTrait.php
--- App\Models\AbstractTier\Phenomenon App\Models\Phenomenon ---
app/Http/Controllers/API/PhenomenonAPIController.php
app/Http/Requests/API/CreatePhenomenonAPIRequest.php
app/Http/Requests/API/UpdatePhenomenonAPIRequest.php
app/Http/Requests/CreatePhenomenonRequest.php
app/Http/Requests/UpdatePhenomenonRequest.php
app/Repositories/PhenomenonRepository.php
tests/PhenomenonRepositoryTest.php
tests/traits/MakePhenomenonTrait.php
--- App\Models\AbstractTier\Resource App\Models\Resource ---
app/Http/Controllers/API/ResourceAPIController.php
app/Http/Requests/API/CreateResourceAPIRequest.php
app/Http/Requests/API/UpdateResourceAPIRequest.php
app/Http/Requests/CreateResourceRequest.php
app/Http/Requests/UpdateResourceRequest.php
app/Repositories/ResourceRepository.php
tests/ResourceRepositoryTest.php
tests/traits/MakeResourceTrait.php
--- App\Models\SimulationTier\ResultAnalytic App\Models\ResultAnalytic ---
app/Http/Controllers/API/ResultAnalyticAPIController.php
app/Http/Requests/API/CreateResultAnalyticAPIRequest.php
app/Http/Requests/API/UpdateResultAnalyticAPIRequest.php
app/Http/Requests/CreateResultAnalyticRequest.php
app/Http/Requests/UpdateResultAnalyticRequest.php
app/Repositories/ResultAnalyticRepository.php
tests/ResultAnalyticRepositoryTest.php
tests/traits/MakeResultAnalyticTrait.php
--- App\Models\SimulationTier\ResultFeatureChunk App\Models\ResultFeatureChunk ---
app/Http/Controllers/API/ResultFeatureChunkAPIController.php
app/Http/Requests/API/CreateResultFeatureChunkAPIRequest.php
app/Http/Requests/API/UpdateResultFeatureChunkAPIRequest.php
app/Http/Requests/CreateResultFeatureChunkRequest.php
app/Http/Requests/UpdateResultFeatureChunkRequest.php
app/Repositories/ResultFeatureChunkRepository.php
tests/ResultFeatureChunkRepositoryTest.php
tests/traits/MakeResultFeatureChunkTrait.php
--- App\Models\SimulationTier\ResultResource App\Models\ResultResource ---
app/Http/Controllers/API/ResultResourceAPIController.php
app/Http/Requests/API/CreateResultResourceAPIRequest.php
app/Http/Requests/API/UpdateResultResourceAPIRequest.php
app/Http/Requests/CreateResultResourceRequest.php
app/Http/Requests/UpdateResultResourceRequest.php
app/Repositories/ResultResourceRepository.php
tests/ResultResourceRepositoryTest.php
tests/traits/MakeResultResourceTrait.php
--- App\Models\Users\Role App\Models\Role ---
app/Http/Controllers/API/RoleAPIController.php
app/Http/Requests/API/CreateRoleAPIRequest.php
app/Http/Requests/API/UpdateRoleAPIRequest.php
app/Http/Requests/CreateRoleRequest.php
app/Http/Requests/UpdateRoleRequest.php
app/Repositories/RoleRepository.php
tests/RoleRepositoryTest.php
tests/traits/MakeRoleTrait.php
--- App\Models\SimulationTier\Simulation App\Models\Simulation ---
app/Http/Controllers/API/SimulationAPIController.php
app/Http/Requests/API/CreateSimulationAPIRequest.php
app/Http/Requests/API/UpdateSimulationAPIRequest.php
app/Http/Requests/CreateSimulationRequest.php
app/Http/Requests/UpdateSimulationRequest.php
app/Repositories/SimulationRepository.php
tests/SimulationRepositoryTest.php
tests/traits/MakeSimulationTrait.php
--- App\Models\Users\User App\Models\User ---
app/Http/Controllers/API/UserAPIController.php
app/Http/Requests/API/CreateUserAPIRequest.php
app/Http/Requests/API/UpdateUserAPIRequest.php
app/Http/Requests/CreateUserRequest.php
app/Http/Requests/UpdateUserRequest.php
app/Repositories/UserRepository.php
tests/traits/MakeUserTrait.php
tests/UserRepositoryTest.php
--- App\Models\Interaction\WorksheetAnswer App\Models\WorksheetAnswer ---
app/Http/Controllers/API/WorksheetAnswerAPIController.php
app/Http/Requests/API/CreateWorksheetAnswerAPIRequest.php
app/Http/Requests/API/UpdateWorksheetAnswerAPIRequest.php
app/Http/Requests/CreateWorksheetAnswerRequest.php
app/Http/Requests/UpdateWorksheetAnswerRequest.php
app/Repositories/WorksheetAnswerRepository.php
tests/traits/MakeWorksheetAnswerTrait.php
tests/WorksheetAnswerRepositoryTest.php
--- App\Models\Interaction\Worksheet App\Models\Worksheet ---
app/Http/Controllers/API/WorksheetAnswerAPIController.php
app/Http/Controllers/API/WorksheetAPIController.php
app/Http/Requests/API/CreateWorksheetAnswerAPIRequest.php
app/Http/Requests/API/CreateWorksheetAPIRequest.php
app/Http/Requests/API/UpdateWorksheetAnswerAPIRequest.php
app/Http/Requests/API/UpdateWorksheetAPIRequest.php
app/Http/Requests/CreateWorksheetAnswerRequest.php
app/Http/Requests/CreateWorksheetRequest.php
app/Http/Requests/UpdateWorksheetAnswerRequest.php
app/Http/Requests/UpdateWorksheetRequest.php
app/Repositories/WorksheetAnswerRepository.php
app/Repositories/WorksheetRepository.php
tests/traits/MakeWorksheetAnswerTrait.php
tests/traits/MakeWorksheetTrait.php
tests/WorksheetAnswerRepositoryTest.php
tests/WorksheetRepositoryTest.php
