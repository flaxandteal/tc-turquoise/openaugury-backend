<?php

return [
  'auth_login_redirect_path' => '/admin',
  'admin_app_url' => env('TWILL_ADMIN_APP_URL', 'localhost'),
  'users_table' => 'users',
  'admin_app_path' => 'admin',
  'custom_components_resource_path' => 'assets/js/components/admin',
  'enabled' => [
      'activitylog' => false,
      'users-management' => false,
      'file-library' => true
  ],
  'locale' => 'en', // changing this will make your locale the default for all CMS users
  'fallback_locale' => 'en',
  'available_user_locales' => [ // list of locales that CMS users can use, add your new locale here if you need another one
    'en_GB',
    'ru_RU',
    'ar_SA',
  ],
  'file_library' => [
    'allowed_extensions' => ['pdf','txt','doc'],
    'disk' => env('FILE_LIBRARY_DISK', 'twill-s3'),
    'endpoint_type' => env('FILE_LIBRARY_ENDPOINT_TYPE', 's3'),
    'cascade_delete' => env('FILE_LIBRARY_CASCADE_DELETE', true),
    'file_service' => env('FILE_LIBRARY_FILE_SERVICE', 'A17\Twill\Services\FileLibrary\Disk'),
    'acl' => env('FILE_LIBRARY_ACL', 'public-read'),
    'prefix_uuid_with_local_path' => false
],
  'block_editor' => [
    'files' => ['newsFile', 'taskFile']
  ],
  
];
