* extent setting removed from Simulation::updateFromCase, as it's missing the ST_GeogFromText when persisting to PostGIS, but only on creation (updating is fine)
